with Ada.Text_IO;
with Ada.Numerics.Real_Arrays;

with CUDA.Autoinit;
with CUDA.Compiler;

pragma Unreferenced (CUDA.Autoinit);

procedure Add
is
   use CUDA;
   use Ada.Numerics.Real_Arrays;

   package Real_Vector_Args is new Compiler.Arg_Creators
     (Data_Type => Ada.Numerics.Real_Arrays.Real_Vector);
   use Real_Vector_Args;

   N : constant := 32 * 1024;

   A      : Real_Vector         := (1 .. N => 2.0);
   B      : Real_Vector         := (1 .. N => 2.0);
   C      : aliased Real_Vector := (1 .. N => 0.0);
   Src    : Compiler.Source_Module_Type;
   Func   : Compiler.Function_Type;
   Module : Compiler.Module_Type;
begin
   Src := Compiler.Create
     (Preamble  => "#define N" & N'Img,
      Operation =>
      "__global__ void add(float *a, float *b, float *c) {"
      & "   int tid = blockIdx.x;"
      & "   while (tid < N) {"
      & "        c[tid] = a[tid] + b[tid];"
      & "        tid += gridDim.x;"
      & "}}");

   Module := Compiler.Compile (Source => Src);
   Func   := Compiler.Get_Function (Module => Module,
                                    Name   => "add");

   Func.Call
     (Args =>
        (1 => In_Arg (Data => A),
         2 => In_Arg (Data => B),
         3 => Out_Arg (Data => C'Access)));
end Add;
