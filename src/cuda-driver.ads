--
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Interfaces.C;

private with driver_types_h;

package CUDA.Driver is

   function Device_Count return Natural;
   --  Return count of available CUDA devices.

   type Device_Type (<>) is private;
   --  CUDA device.

   function Get_Device return Device_Type;
   --  Get CUDA device. The CUDA runtime decides which device is returned.

   procedure Iterate (Process : not null access procedure (Dev : Device_Type));
   --  Iterate over all available CUDA devices.

   function Name (Dev : Device_Type) return String;
   --  Get name of cuda device.

   function Compute_Capability (Dev : Device_Type) return Float;
   --  Get compute capability of device.

   function Clock_Rate (Dev : Device_Type) return Natural;
   --  Get device clock rate.

   function Copy_Overlap (Dev : Device_Type) return Boolean;
   --  Return True if the device can simultaneously perform a cudaMemcpy() and
   --  kernel execution.

   function Kernel_Exec_Timeout (Dev : Device_Type) return Boolean;
   --  Return True if there exists a runtime limit for kernels executed on the
   --  device.

   function Total_Global_Mem (Dev : Device_Type) return Natural;
   --  Return the amount of global memory on the device in bytes.

   function Total_Constant_Mem (Dev : Device_Type) return Natural;
   --  Return the amount of available constant memory on the device (in
   --  bytes).

   function Max_Mem_Pitch (Dev : Device_Type) return Natural;
   --  Return the maximum pitch allowed for memory copies in bytes.

   function Texture_Alignment (Dev : Device_Type) return Natural;
   --  Return the device's requirement for texture alignment.

   function Multiprocessor_Count (Dev : Device_Type) return Natural;
   --  Return the number of multiprocessors on the device.

   function Shared_Mem_Per_Block (Dev : Device_Type) return Natural;
   --  Return the maximum amount of shared memory a single block may use in
   --  bytes.

   function Regs_Per_Block (Dev : Device_Type) return Natural;
   --  Return the number of 32-bit registers available per block.

   function Warp_Size (Dev : Device_Type) return Natural;
   --  Return the number of threads in a warp.

   function Max_Threads_Per_Block (Dev : Device_Type) return Natural;
   --  Return the maximum number of threads that a block may contain.

   type Nat_3_Array is array (1 .. 3) of Natural;

   function Max_Thread_Dimensions (Dev : Device_Type) return Nat_3_Array;
   --  Return the maximum number of threads allowed along each dimension of a
   --  block.

   function Max_Grid_Size (Dev : Device_Type) return Nat_3_Array;
   --  The number of blocks allowed along each dimension of a grid.

private

   type Device_Type is record
      Device_Nr : Interfaces.C.int := 0;
      CU_Props  : aliased driver_types_h.cudaDeviceProp;
   end record;

end CUDA.Driver;
