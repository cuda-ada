--
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Fixed;

with Interfaces.C;

with cuda_runtime_api_h;
with driver_types_h;
with stddef_h;

with CUDA.Driver;

package body Cuda_Driver_Tests is

   use Ahven;
   use CUDA.Driver;

   package IC renames Interfaces.C;

   -------------------------------------------------------------------------

   procedure Get_Device_Attributes
   is
      use type IC.int;
      use type IC.unsigned;      --  i686
      use type IC.unsigned_long; --  x86_64

      CU_Dev   : aliased IC.int;
      CU_Props : aliased driver_types_h.cudaDeviceProp;
      Res      : driver_types_h.cudaError_t;

      Device   : constant Device_Type := Get_Device;
   begin
      Res := cuda_runtime_api_h.cudaGetDevice (arg1 => CU_Dev'Access);
      Assert (Condition => Res = driver_types_h.cudaSuccess,
              Message   => "Could not get device");

      Res := cuda_runtime_api_h.cudaGetDeviceProperties
        (arg1 => CU_Props'Access,
         arg2 => CU_Dev);
      Assert (Condition => Res = driver_types_h.cudaSuccess,
              Message   => "Could not get device properties");

      Assert (Condition => Name (Dev => Device) = IC.To_Ada (CU_Props.name),
              Message   => "Name mismatch");

      declare
         use Ada.Strings;

         Cap     : constant Float  := Compute_Capability (Dev => Device);
         Ref_Img : constant String := Fixed.Trim (CU_Props.major'Img, Left)
           & "." & Fixed.Trim (CU_Props.minor'Img, Left);
         Cap_Img : constant String := Fixed.Trim (Cap'Img, Left);
      begin
         Assert (Condition => Cap_Img (1 .. 3) = Ref_Img,
                 Message   => "Compute capability mismatch");
      end;

      Assert (Condition => IC.int
              (Clock_Rate (Dev => Device)) = CU_Props.clockRate,
              Message   => "Clock rate mismatch");
      Assert (Condition => Boolean'Pos
              (Copy_Overlap (Dev => Device)) = CU_Props.deviceOverlap,
              Message   => "Device copy overlap mismatch");
      Assert (Condition => Boolean'Pos (Kernel_Exec_Timeout
              (Dev => Device)) = CU_Props.kernelExecTimeoutEnabled,
              Message   => "Kernel exec timeout mismatch");
      Assert (Condition => stddef_h.size_t
              (Total_Global_Mem (Dev => Device)) = CU_Props.totalGlobalMem,
              Message   => "Total global mem mismatch");
      Assert (Condition => stddef_h.size_t
              (Total_Constant_Mem (Dev => Device)) = CU_Props.totalConstMem,
              Message   => "Total constant mem mismatch");
      Assert (Condition => stddef_h.size_t
              (Max_Mem_Pitch (Dev => Device)) = CU_Props.memPitch,
              Message   => "Max mem pitch mismatch");
      Assert (Condition => stddef_h.size_t
              (Texture_Alignment (Dev => Device)) = CU_Props.textureAlignment,
              Message   => "Texture alignment mismatch");
      Assert (Condition => IC.int (Multiprocessor_Count
              (Dev => Device)) = CU_Props.multiProcessorCount,
              Message   => "Multiprocessor count mismatch");
      Assert (Condition => stddef_h.size_t (Shared_Mem_Per_Block
              (Dev => Device)) = CU_Props.sharedMemPerBlock,
              Message   => "Shared mem per block mismatch");
      Assert (Condition => IC.int
              (Regs_Per_Block (Dev => Device)) = CU_Props.regsPerBlock,
              Message   => "Regs per block mismatch");
      Assert (Condition => IC.int
              (Warp_Size (Dev => Device)) = CU_Props.warpSize,
              Message   => "Warp size mismatch");
      Assert (Condition => IC.int (Max_Threads_Per_Block
              (Dev => Device)) = CU_Props.maxThreadsPerBlock,
              Message   => "Max threads per block mismatch");

      declare
         Max_Thread_Dim : constant Nat_3_Array
           := Max_Thread_Dimensions (Dev => Device);
      begin
         Assert (Condition => IC.int
                 (Max_Thread_Dim (1)) = CU_Props.maxThreadsDim (0),
                 Message   => "Max thread dim (1) mismatch");
         Assert (Condition => IC.int
                 (Max_Thread_Dim (2)) = CU_Props.maxThreadsDim (1),
                 Message   => "Max thread dim (2) mismatch");
         Assert (Condition => IC.int
                 (Max_Thread_Dim (3)) = CU_Props.maxThreadsDim (2),
                 Message   => "Max thread dim (3) mismatch");
      end;

      declare
         Max_Grid : constant Nat_3_Array := Max_Grid_Size (Dev => Device);
      begin
         Assert (Condition => IC.int (Max_Grid (1)) = CU_Props.maxGridSize (0),
                 Message   => "Max grid (1) mismatch");
         Assert (Condition => IC.int (Max_Grid (2)) = CU_Props.maxGridSize (1),
                 Message   => "Max grid (2) mismatch");
         Assert (Condition => IC.int (Max_Grid (3)) = CU_Props.maxGridSize (2),
                 Message   => "Max grid (3) mismatch");
      end;
   end Get_Device_Attributes;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Driver package");
      T.Add_Test_Routine
        (Routine => Get_Device_Attributes'Access,
         Name    => "Get CUDA device attributes");
   end Initialize;

end Cuda_Driver_Tests;
