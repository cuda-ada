with Interfaces.C; use Interfaces.C;

package device_types_h is

   type cudaRoundMode is
     (cudaRoundNearest,
      cudaRoundZero,
      cudaRoundPosInf,
      cudaRoundMinInf);
   pragma Convention (C, cudaRoundMode);  -- device_types.h:60

end device_types_h;
