PREFIX ?= $(HOME)/libraries
CUDASDK = /usr/local/cuda
OBJDIR  = obj
LIBDIR  = lib
SRCDIR  = src
THINDIR = thin
COVDIR  = $(OBJDIR)/cov
PERFDIR = perf

GPR_FILES = gnat/*.gpr

LIBRARY_PATH = $(CUDASDK)/lib:$(CUDASDK)/lib64

NUM_CPUS := $(shell getconf _NPROCESSORS_ONLN)
ARCH     ?= $(shell uname -m)

GMAKE_OPTS = -p -R -j$(NUM_CPUS)

MAJOR    = 0
MINOR    = 1
VERSION  = $(MAJOR).$(MINOR)
CUDA_ADA = libcudaada-$(VERSION)
TARBALL  = $(CUDA_ADA).tar.bz2

SO_LIBRARY   = libcudaada.so.$(VERSION)
LIBRARY_KIND = dynamic

SOURCES = \
	$(SRCDIR)/*.ad[bs] \
	$(THINDIR)/*.ad[bs] \
	$(THINDIR)/$(ARCH)/*.ad[bs]
ALIFILES = \
	$(LIBDIR)/*.ali \
	$(OBJDIR)/$(THINDIR)/*.ali

all: build_lib

build_lib:
	@LIBRARY_PATH=$(LIBRARY_PATH) gnatmake -p -Pcuda_lib \
		-XARCH=$(ARCH) -XVERSION="$(VERSION)" -XLIBRARY_KIND="$(LIBRARY_KIND)"

build_examples:
	@LIBRARY_PATH=$(LIBRARY_PATH) gnatmake -p -Pcuda_examples -XARCH=$(ARCH)

build_tests:
	@LIBRARY_PATH=$(LIBRARY_PATH) gnatmake $(GMAKE_OPTS) -Pcuda_tests.gpr \
		-XARCH=$(ARCH)

install: install_lib install_$(LIBRARY_KIND)

install_lib: build_lib
	install -d $(PREFIX)/include/cuda-ada
	install -d $(PREFIX)/lib/cuda-ada
	install -d $(PREFIX)/lib/gnat
	install -m 644 $(SOURCES) $(PREFIX)/include/cuda-ada
	install -m 444 $(ALIFILES) $(PREFIX)/lib/cuda-ada
	install -m 644 $(GPR_FILES) $(PREFIX)/lib/gnat

install_static:
	install -m 444 $(LIBDIR)/libcudaada.a $(PREFIX)/lib

install_dynamic:
	install -m 444 $(LIBDIR)/$(SO_LIBRARY) $(PREFIX)/lib
	cd $(PREFIX)/lib && ln -sf $(SO_LIBRARY) libcudaada.so

tests: build_tests
	@LD_LIBRARY_PATH=$(LIBRARY_PATH) $(OBJDIR)/test_runner

doc:
	$(MAKE) -C doc

cov:
	@rm -f $(COVDIR)/*.gcda
	@LIBRARY_PATH=$(LIBRARY_PATH) gnatmake $(GMAKE_OPTS) -Pcuda_tests.gpr \
		-XARCH=$(ARCH) -XBUILD="coverage"
	@LD_LIBRARY_PATH=$(LIBRARY_PATH) $(COVDIR)/test_runner || true
	@lcov -c -d $(COVDIR) -o $(COVDIR)/cov.info
	@lcov -e $(COVDIR)/cov.info "$(PWD)/src/*.adb" -o $(COVDIR)/cov.info
	@genhtml --no-branch-coverage $(COVDIR)/cov.info -o $(COVDIR)

perf: $(OBJDIR)/perf_c $(OBJDIR)/perf_c_drv
	@LIBRARY_PATH=$(LIBRARY_PATH) gnatmake $(GMAKE_OPTS) -Pcuda_perf.gpr \
		-XARCH=$(ARCH)
	@LD_LIBRARY_PATH=$(LIBRARY_PATH) $(OBJDIR)/perf_cudaada $(COUNT)
	@LD_LIBRARY_PATH=$(LIBRARY_PATH) $(OBJDIR)/perf_c $(COUNT)
	@LD_LIBRARY_PATH=$(LIBRARY_PATH) $(OBJDIR)/perf_c_drv $(COUNT)

$(OBJDIR)/perf_c: $(PERFDIR)/perf_c.cu
	@mkdir -p $(OBJDIR)
	LIBRARY_PATH=$(LIBRARY_PATH) $(CUDASDK)/bin/nvcc $< -o $@

$(OBJDIR)/perf_c_drv: $(PERFDIR)/perf_c_drv.c
	@mkdir -p $(OBJDIR)
	LIBRARY_PATH=$(LIBRARY_PATH) $(CC) $< -o $@ -I$(CUDASDK)/include -lcuda

clean:
	@rm -rf cache
	@rm -rf $(OBJDIR)
	@rm -rf $(LIBDIR)
	$(MAKE) -C doc clean

dist:
	@echo "Creating release tarball $(TARBALL) ... "
	@git archive --format=tar HEAD --prefix $(CUDA_ADA)/ | bzip2 > $(TARBALL)

.PHONY: all build_examples build_lib build_tests clean dist doc install \
	install_dynamic install_lib install_static perf tests
