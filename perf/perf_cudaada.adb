--
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--  This software contains source code provided by NVIDIA Corporation.
--  The CUDA kernel for the matrix multiplication is based on the source
--  provided in NVIDIA SDK code sample, see:
--    http://developer.nvidia.com/cuda-cc-sdk-code-samples#matrixMul

with Ada.Text_IO;
with Ada.Real_Time;
with Ada.Exceptions;
with Ada.Command_Line;
with Ada.Numerics.Real_Arrays;

with CUDA.Autoinit;
with CUDA.Compiler;

pragma Unreferenced (CUDA.Autoinit);

procedure Perf_CudaAda
is
   package Real_Matrix_Args is new CUDA.Compiler.Arg_Creators
     (Data_Type => Ada.Numerics.Real_Arrays.Real_Matrix);

   use CUDA;
   use Real_Matrix_Args;

   Iterations : Positive := 1;
   Exec_Time  : Ada.Real_Time.Time_Span;

   N          : constant := 512;
   Blocksize  : constant := 16;
   Gridsize   : constant Positive
     := Positive (Float'Ceiling (Float (N) / Float (Blocksize)));

   A      : constant Ada.Numerics.Real_Arrays.Real_Matrix (1 .. N, 1 .. N)
     := (others => (others => 2.0));
   B      : constant Ada.Numerics.Real_Arrays.Real_Matrix (1 .. N, 1 .. N)
     := (others => (others => 2.0));
   C      : aliased Ada.Numerics.Real_Arrays.Real_Matrix
     := (1 .. N => (1 .. N => 0.0));
   Add_Src, Mul_Src       : Compiler.Source_Module_Type;
   Add_Func, Mul_Func     : Compiler.Function_Type;
   Add_Module, Mul_Module : Compiler.Module_Type;

   procedure CPU_Add;
   --  Execute matrix addition on CPU.

   procedure CPU_Mul;
   --  Execute matrix multiplication on CPU.

   procedure GPU_Add;
   --  Execute matrix addition on GPU.

   procedure GPU_Mul;
   --  Execute matrix multiplication on GPU.

   procedure Profile
     (Process   : not null access procedure;
      Exec_Time : out Ada.Real_Time.Time_Span);
   --  Execute the given process procedure 'iterations' times and return the
   --  cumulated execution time.

   -------------------------------------------------------------------------

   procedure CPU_Add
   is
      use type Ada.Numerics.Real_Arrays.Real_Matrix;
   begin
      C := A + B;
   end CPU_Add;

   -------------------------------------------------------------------------

   procedure CPU_Mul
   is
      use type Ada.Numerics.Real_Arrays.Real_Matrix;
   begin
      C := A * B;
   end CPU_Mul;

   -------------------------------------------------------------------------

   procedure GPU_Add
   is
   begin
      Add_Func.Call
        (Args        => (1 => In_Arg (Data => A),
                         2 => In_Arg (Data => B),
                         3 => Out_Arg (Data => C'Access)),
         Grid_Dim_X  => Gridsize,
         Grid_Dim_Y  => Gridsize,
         Block_Dim_X => Blocksize,
         Block_Dim_Y => Blocksize);
   end GPU_Add;

   -------------------------------------------------------------------------

   procedure GPU_Mul
   is
   begin
      Mul_Func.Call
        (Args        => (1 => In_Arg (Data => A),
                         2 => In_Arg (Data => B),
                         3 => Out_Arg (Data => C'Access)),
         Grid_Dim_X  => Gridsize,
         Grid_Dim_Y  => Gridsize,
         Block_Dim_X => Blocksize,
         Block_Dim_Y => Blocksize);
   end GPU_Mul;

   -------------------------------------------------------------------------

   procedure Profile
     (Process   : not null access procedure;
      Exec_Time : out Ada.Real_Time.Time_Span)
   is
      use Ada.Real_Time;

      Start_Time : Time;
   begin
      Exec_Time := Time_Span_Zero;

      for I in Positive range 1 .. Iterations loop
         Start_Time := Ada.Real_Time.Clock;
         Process.all;
         Exec_Time := Exec_Time + (Clock - Start_Time);
      end loop;
   end Profile;

begin
   if Ada.Command_Line.Argument_Count = 1 then
      begin
         Iterations := Positive'Value
           (Ada.Command_Line.Argument
              (Number => 1));

      exception
         when Constraint_Error =>
            Ada.Text_IO.Put_Line ("Invalid iteration count");
            Ada.Command_Line.Set_Exit_Status
              (Code => Ada.Command_Line.Failure);
            return;
      end;
   end if;

   Add_Src := Compiler.Create
     (Preamble  => "#define N" & N'Img,
      Operation => "__global__ void add( float *a, float *b, float *c ) {" &
      "   int i = blockIdx.x * blockDim.x + threadIdx.x;"                  &
      "   int j = blockIdx.y * blockDim.y + threadIdx.y;"                  &
      "   int idx = i + j * N;"                                            &
      "   if (i < N && j < N) {"                                           &
      "      c[idx] = a[idx] + b[idx];"                                    &
      "   }"                                                               &
      "}");
   Add_Module := Compiler.Compile (Source => Add_Src);
   Add_Func   := Compiler.Get_Function (Module => Add_Module,
                                        Name   => "add");

   Mul_Src := Compiler.Create
     (Preamble  => "#define N" & N'Img & ASCII.LF
      & "#define BLOCK_SIZE" & Blocksize'Img,
      Operation => "__global__ void mul( float* A, float* B, float* C) {" &
      "   int bx = blockIdx.x;"                                           &
      "   int by = blockIdx.y;"                                           &
      "   int tx = threadIdx.x;"                                          &
      "   int ty = threadIdx.y;"                                          &
      "   int aBegin = N * BLOCK_SIZE * by;"                              &
      "   int aEnd   = aBegin + N - 1;"                                   &
      "   int aStep  = BLOCK_SIZE;"                                       &
      "   int bBegin = BLOCK_SIZE * bx;"                                  &
      "   int bStep  = BLOCK_SIZE * N;"                                   &
      "   float Csub = 0;"                                                &
      "   for (int a = aBegin, b = bBegin;"                               &
      "        a <= aEnd;"                                                &
      "        a += aStep, b += bStep) {"                                 &
      "      __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];"                &
      "      __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];"                &
      "      As[ty][tx] = A[a + N * ty + tx];"                            &
      "      Bs[ty][tx] = B[b + N * ty + tx];"                            &
      "      __syncthreads();"                                            &
      "      for (int k = 0; k < BLOCK_SIZE; ++k)"                        &
      "         Csub += As[ty][k] * Bs[k][tx];"                           &
      "      __syncthreads();"                                            &
      "   }"                                                              &
      "   int c = N * BLOCK_SIZE * by + BLOCK_SIZE * bx;"                 &
      "   C[c + N * ty + tx] = Csub;"                                     &
      "}");
   Mul_Module := Compiler.Compile (Source => Mul_Src);
   Mul_Func   := Compiler.Get_Function (Module => Mul_Module,
                                        Name   => "mul");

   Ada.Text_IO.Put_Line ("Executing" & Iterations'Img & " iteration(s)");
   Ada.Text_IO.Put_Line ("- Matrix [1 .." & N'Img & ", 1 .." & N'Img & "]");
   Ada.Text_IO.Put_Line ("- Grid size" & Gridsize'Img);
   Ada.Text_IO.Put_Line ("- Block size" & Blocksize'Img);

   Profile (Process   => CPU_Add'Access,
            Exec_Time => Exec_Time);
   Ada.Text_IO.Put_Line ("Total CPU Addition execution time:"
                         & Ada.Real_Time.To_Duration (TS => Exec_Time)'Img);
   Profile (Process   => GPU_Add'Access,
            Exec_Time => Exec_Time);
   Ada.Text_IO.Put_Line ("Total GPU Addition execution time:"
                         & Ada.Real_Time.To_Duration (TS => Exec_Time)'Img);
   Profile (Process   => CPU_Mul'Access,
            Exec_Time => Exec_Time);
   Ada.Text_IO.Put_Line ("Total CPU Multiplication execution time:"
                         & Ada.Real_Time.To_Duration (TS => Exec_Time)'Img);
   Profile (Process   => GPU_Mul'Access,
            Exec_Time => Exec_Time);
   Ada.Text_IO.Put_Line ("Total GPU Multiplication execution time:"
                         & Ada.Real_Time.To_Duration (TS => Exec_Time)'Img);

   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when E : others =>
      Ada.Text_IO.Put_Line ("Execution error:");
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end Perf_CudaAda;
