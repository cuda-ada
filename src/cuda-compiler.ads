--
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Ada.Finalization;
private with Ada.Strings.Unbounded;

private with System;

private with stddef_h;
private with cuda_h;

package CUDA.Compiler is

   type Source_Module_Type is private;
   --  Source module. Stores CUDA source code.

   function Create
     (Preamble  : String := "";
      Operation : String)
      return Source_Module_Type;
   --  Create source module from given preamble and operation code.

   function Get_Code (Src_Module : Source_Module_Type) return String;
   --  Return CUDA source module code as string.

   type Module_Type is private;
   --  CUDA module type.

   function Compile (Source : Source_Module_Type) return Module_Type;
   --  Compile given CUDA source module.

   type Function_Type is tagged private;
   --  CUDA function.

   function Get_Function
     (Module : Module_Type;
      Name   : String)
      return Function_Type;
   --  Return CUDA function with given name from compiled module.

   type Arg_Type is limited private;
   --  CUDA function argument type.

   generic
      type Data_Type (<>) is private;
   package Arg_Creators is

      function In_Arg (Data : Data_Type) return Arg_Type;
      --  Returns an in-mode argument object for the given data type. Device
      --  memory is allocated and initialized with the specified data.

      function Out_Arg (Data : not null access Data_Type) return Arg_Type;
      --  Returns an out-mode argument object for the given data type. Device
      --  memory is allocated. Results are copied back from the device to the
      --  given data object once the kernel has finished its execution.

      function In_Out_Arg (Data : not null access Data_Type) return Arg_Type;
      --  Returns an in-mode argument object for the given data type. Device
      --  memory is allocated and initialized with the specified data. Results
      --  are copied back from the device to the given data object once the
      --  kernel has finished its execution.

   end Arg_Creators;

   type Kernel_Args_Type is array (Positive range <>) of Arg_Type;
   --  CUDA kernel arguments.

   procedure Call
     (Func        : Function_Type;
      Args        : Kernel_Args_Type;
      Grid_Dim_X  : Positive := 128;
      Grid_Dim_Y  : Positive := 1;
      Grid_Dim_Z  : Positive := 1;
      Block_Dim_X : Positive := 1;
      Block_Dim_Y : Positive := 1;
      Block_Dim_Z : Positive := 1);
   --  Call CUDA function with given arguments. Optionally the caller can
   --  specify the grid and block dimensions. The kernel is executed on a
   --  Grid_Dim_X x Grid_Dim_Y x Grid_Dim_Z grid of blocks. Each block contains
   --  Block_Dim_X x Block_Dim_Y x Block_Dim_Z threads.

private

   type Source_Module_Type is record
      Code : Ada.Strings.Unbounded.Unbounded_String;
   end record;

   type Arg_Type is new Ada.Finalization.Limited_Controlled with record
      Dev_Data  : aliased cuda_h.CUdeviceptr;
      --  Device memory
      Host_Data : System.Address  := System.Null_Address;
      --  Host data
      Data_Size : stddef_h.size_t := 0;
      --  Memory size of data in bytes
   end record;

   procedure Finalize (Object : in out Arg_Type);
   --  Deallocate device memory and copy device data back to host if needed.

   type Module_Type is record
      CU_Module : cuda_h.CUmodule;
      --  CUDA module
   end record;

   type Function_Type is tagged record
      CU_Function : cuda_h.CUfunction;
      --- CUDA function
   end record;

end CUDA.Compiler;
