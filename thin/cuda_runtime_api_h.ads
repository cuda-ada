with Interfaces.C; use Interfaces.C;
with driver_types_h;
with stddef_h;
with Interfaces.C.Strings;
with System;
with vector_types_h;

package cuda_runtime_api_h is


   CUDART_VERSION : constant := 4000;  --  cuda_runtime_api.h:82

   function cudaDeviceReset return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:134
   pragma Import (C, cudaDeviceReset, "cudaDeviceReset");

   function cudaDeviceSynchronize return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:151
   pragma Import (C, cudaDeviceSynchronize, "cudaDeviceSynchronize");

   function cudaDeviceSetLimit (arg1 : driver_types_h.cudaLimit; arg2 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:203
   pragma Import (C, cudaDeviceSetLimit, "cudaDeviceSetLimit");

   function cudaDeviceGetLimit (arg1 : access stddef_h.size_t; arg2 : driver_types_h.cudaLimit) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:227
   pragma Import (C, cudaDeviceGetLimit, "cudaDeviceGetLimit");

   function cudaDeviceGetCacheConfig (arg1 : access driver_types_h.cudaFuncCache) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:257
   pragma Import (C, cudaDeviceGetCacheConfig, "cudaDeviceGetCacheConfig");

   function cudaDeviceSetCacheConfig (arg1 : driver_types_h.cudaFuncCache) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:298
   pragma Import (C, cudaDeviceSetCacheConfig, "cudaDeviceSetCacheConfig");

   function cudaThreadExit return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:332
   pragma Import (C, cudaThreadExit, "cudaThreadExit");

   function cudaThreadSynchronize return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:356
   pragma Import (C, cudaThreadSynchronize, "cudaThreadSynchronize");

   function cudaThreadSetLimit (arg1 : driver_types_h.cudaLimit; arg2 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:415
   pragma Import (C, cudaThreadSetLimit, "cudaThreadSetLimit");

   function cudaThreadGetLimit (arg1 : access stddef_h.size_t; arg2 : driver_types_h.cudaLimit) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:446
   pragma Import (C, cudaThreadGetLimit, "cudaThreadGetLimit");

   function cudaThreadGetCacheConfig (arg1 : access driver_types_h.cudaFuncCache) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:481
   pragma Import (C, cudaThreadGetCacheConfig, "cudaThreadGetCacheConfig");

   function cudaThreadSetCacheConfig (arg1 : driver_types_h.cudaFuncCache) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:527
   pragma Import (C, cudaThreadSetCacheConfig, "cudaThreadSetCacheConfig");

   function cudaGetLastError return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:581
   pragma Import (C, cudaGetLastError, "cudaGetLastError");

   function cudaPeekAtLastError return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:624
   pragma Import (C, cudaPeekAtLastError, "cudaPeekAtLastError");

   function cudaGetErrorString (arg1 : driver_types_h.cudaError_t) return Interfaces.C.Strings.chars_ptr;  -- cuda_runtime_api.h:638
   pragma Import (C, cudaGetErrorString, "cudaGetErrorString");

   function cudaGetDeviceCount (arg1 : access int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:668
   pragma Import (C, cudaGetDeviceCount, "cudaGetDeviceCount");

   function cudaGetDeviceProperties (arg1 : access driver_types_h.cudaDeviceProp; arg2 : int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:829
   pragma Import (C, cudaGetDeviceProperties, "cudaGetDeviceProperties");

   function cudaChooseDevice (arg1 : access int; arg2 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:848
   pragma Import (C, cudaChooseDevice, "cudaChooseDevice");

   function cudaSetDevice (arg1 : int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:881
   pragma Import (C, cudaSetDevice, "cudaSetDevice");

   function cudaGetDevice (arg1 : access int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:898
   pragma Import (C, cudaGetDevice, "cudaGetDevice");

   function cudaSetValidDevices (arg1 : access int; arg2 : int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:927
   pragma Import (C, cudaSetValidDevices, "cudaSetValidDevices");

   function cudaSetDeviceFlags (arg1 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:987
   pragma Import (C, cudaSetDeviceFlags, "cudaSetDeviceFlags");

   function cudaStreamCreate (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1013
   pragma Import (C, cudaStreamCreate, "cudaStreamCreate");

   function cudaStreamDestroy (arg1 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1034
   pragma Import (C, cudaStreamDestroy, "cudaStreamDestroy");

   function cudaStreamWaitEvent
     (arg1 : driver_types_h.cudaStream_t;
      arg2 : driver_types_h.cudaEvent_t;
      arg3 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1070
   pragma Import (C, cudaStreamWaitEvent, "cudaStreamWaitEvent");

   function cudaStreamSynchronize (arg1 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1089
   pragma Import (C, cudaStreamSynchronize, "cudaStreamSynchronize");

   function cudaStreamQuery (arg1 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1107
   pragma Import (C, cudaStreamQuery, "cudaStreamQuery");

   function cudaEventCreate (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1139
   pragma Import (C, cudaEventCreate, "cudaEventCreate");

   function cudaEventCreateWithFlags (arg1 : System.Address; arg2 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1170
   pragma Import (C, cudaEventCreateWithFlags, "cudaEventCreateWithFlags");

   function cudaEventRecord (arg1 : driver_types_h.cudaEvent_t; arg2 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1203
   pragma Import (C, cudaEventRecord, "cudaEventRecord");

   function cudaEventQuery (arg1 : driver_types_h.cudaEvent_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1232
   pragma Import (C, cudaEventQuery, "cudaEventQuery");

   function cudaEventSynchronize (arg1 : driver_types_h.cudaEvent_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1264
   pragma Import (C, cudaEventSynchronize, "cudaEventSynchronize");

   function cudaEventDestroy (arg1 : driver_types_h.cudaEvent_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1289
   pragma Import (C, cudaEventDestroy, "cudaEventDestroy");

   function cudaEventElapsedTime
     (arg1 : access float;
      arg2 : driver_types_h.cudaEvent_t;
      arg3 : driver_types_h.cudaEvent_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1330
   pragma Import (C, cudaEventElapsedTime, "cudaEventElapsedTime");

   function cudaConfigureCall
     (arg1 : vector_types_h.dim3;
      arg2 : vector_types_h.dim3;
      arg3 : stddef_h.size_t;
      arg4 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1369
   pragma Import (C, cudaConfigureCall, "cudaConfigureCall");

   function cudaSetupArgument
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1396
   pragma Import (C, cudaSetupArgument, "cudaSetupArgument");

   function cudaFuncSetCacheConfig (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : driver_types_h.cudaFuncCache) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1442
   pragma Import (C, cudaFuncSetCacheConfig, "cudaFuncSetCacheConfig");

   function cudaLaunch (arg1 : Interfaces.C.Strings.chars_ptr) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1477
   pragma Import (C, cudaLaunch, "cudaLaunch");

   function cudaFuncGetAttributes (arg1 : access driver_types_h.cudaFuncAttributes; arg2 : Interfaces.C.Strings.chars_ptr) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1510
   pragma Import (C, cudaFuncGetAttributes, "cudaFuncGetAttributes");

   function cudaSetDoubleForDevice (arg1 : access double) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1532
   pragma Import (C, cudaSetDoubleForDevice, "cudaSetDoubleForDevice");

   function cudaSetDoubleForHost (arg1 : access double) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1554
   pragma Import (C, cudaSetDoubleForHost, "cudaSetDoubleForHost");

   function cudaMalloc (arg1 : System.Address; arg2 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1586
   pragma Import (C, cudaMalloc, "cudaMalloc");

   function cudaMallocHost (arg1 : System.Address; arg2 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1615
   pragma Import (C, cudaMallocHost, "cudaMallocHost");

   function cudaMallocPitch
     (arg1 : System.Address;
      arg2 : access stddef_h.size_t;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1654
   pragma Import (C, cudaMallocPitch, "cudaMallocPitch");

   function cudaMallocArray
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1693
   pragma Import (C, cudaMallocArray, "cudaMallocArray");

   function cudaFree (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1717
   pragma Import (C, cudaFree, "cudaFree");

   function cudaFreeHost (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1737
   pragma Import (C, cudaFreeHost, "cudaFreeHost");

   function cudaFreeArray (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1759
   pragma Import (C, cudaFreeArray, "cudaFreeArray");

   function cudaHostAlloc
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1818
   pragma Import (C, cudaHostAlloc, "cudaHostAlloc");

   function cudaHostRegister
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1874
   pragma Import (C, cudaHostRegister, "cudaHostRegister");

   function cudaHostUnregister (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1893
   pragma Import (C, cudaHostUnregister, "cudaHostUnregister");

   function cudaHostGetDevicePointer
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1920
   pragma Import (C, cudaHostGetDevicePointer, "cudaHostGetDevicePointer");

   function cudaHostGetFlags (arg1 : access unsigned; arg2 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1939
   pragma Import (C, cudaHostGetFlags, "cudaHostGetFlags");

   function cudaMalloc3D (arg1 : access driver_types_h.cudaPitchedPtr; arg2 : driver_types_h.cudaExtent) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:1974
   pragma Import (C, cudaMalloc3D, "cudaMalloc3D");

   function cudaMalloc3DArray
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : driver_types_h.cudaExtent;
      arg4 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2035
   pragma Import (C, cudaMalloc3DArray, "cudaMalloc3DArray");

   function cudaMemcpy3D (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2132
   pragma Import (C, cudaMemcpy3D, "cudaMemcpy3D");

   function cudaMemcpy3DPeer (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2159
   pragma Import (C, cudaMemcpy3DPeer, "cudaMemcpy3DPeer");

   function cudaMemcpy3DAsync (arg1 : System.Address; arg2 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2264
   pragma Import (C, cudaMemcpy3DAsync, "cudaMemcpy3DAsync");

   function cudaMemcpy3DPeerAsync (arg1 : System.Address; arg2 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2285
   pragma Import (C, cudaMemcpy3DPeerAsync, "cudaMemcpy3DPeerAsync");

   function cudaMemGetInfo (arg1 : access stddef_h.size_t; arg2 : access stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2304
   pragma Import (C, cudaMemGetInfo, "cudaMemGetInfo");

   function cudaMemcpy
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2337
   pragma Import (C, cudaMemcpy, "cudaMemcpy");

   function cudaMemcpyPeer
     (arg1 : System.Address;
      arg2 : int;
      arg3 : System.Address;
      arg4 : int;
      arg5 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2368
   pragma Import (C, cudaMemcpyPeer, "cudaMemcpyPeer");

   function cudaMemcpyToArray
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : stddef_h.size_t;
      arg4 : System.Address;
      arg5 : stddef_h.size_t;
      arg6 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2401
   pragma Import (C, cudaMemcpyToArray, "cudaMemcpyToArray");

   function cudaMemcpyFromArray
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2434
   pragma Import (C, cudaMemcpyFromArray, "cudaMemcpyFromArray");

   function cudaMemcpyArrayToArray
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : stddef_h.size_t;
      arg4 : System.Address;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : stddef_h.size_t;
      arg8 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2469
   pragma Import (C, cudaMemcpyArrayToArray, "cudaMemcpyArrayToArray");

   function cudaMemcpy2D
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : System.Address;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2511
   pragma Import (C, cudaMemcpy2D, "cudaMemcpy2D");

   function cudaMemcpy2DToArray
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : stddef_h.size_t;
      arg4 : System.Address;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : stddef_h.size_t;
      arg8 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2552
   pragma Import (C, cudaMemcpy2DToArray, "cudaMemcpy2DToArray");

   function cudaMemcpy2DFromArray
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : System.Address;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : stddef_h.size_t;
      arg8 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2593
   pragma Import (C, cudaMemcpy2DFromArray, "cudaMemcpy2DFromArray");

   function cudaMemcpy2DArrayToArray
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : stddef_h.size_t;
      arg4 : System.Address;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : stddef_h.size_t;
      arg8 : stddef_h.size_t;
      arg9 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2632
   pragma Import (C, cudaMemcpy2DArrayToArray, "cudaMemcpy2DArrayToArray");

   function cudaMemcpyToSymbol
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2667
   pragma Import (C, cudaMemcpyToSymbol, "cudaMemcpyToSymbol");

   function cudaMemcpyFromSymbol
     (arg1 : System.Address;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : driver_types_h.cudaMemcpyKind) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2701
   pragma Import (C, cudaMemcpyFromSymbol, "cudaMemcpyFromSymbol");

   function cudaMemcpyAsync
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : driver_types_h.cudaMemcpyKind;
      arg5 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2744
   pragma Import (C, cudaMemcpyAsync, "cudaMemcpyAsync");

   function cudaMemcpyPeerAsync
     (arg1 : System.Address;
      arg2 : int;
      arg3 : System.Address;
      arg4 : int;
      arg5 : stddef_h.size_t;
      arg6 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2774
   pragma Import (C, cudaMemcpyPeerAsync, "cudaMemcpyPeerAsync");

   function cudaMemcpyToArrayAsync
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : stddef_h.size_t;
      arg4 : System.Address;
      arg5 : stddef_h.size_t;
      arg6 : driver_types_h.cudaMemcpyKind;
      arg7 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2816
   pragma Import (C, cudaMemcpyToArrayAsync, "cudaMemcpyToArrayAsync");

   function cudaMemcpyFromArrayAsync
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : driver_types_h.cudaMemcpyKind;
      arg7 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2858
   pragma Import (C, cudaMemcpyFromArrayAsync, "cudaMemcpyFromArrayAsync");

   function cudaMemcpy2DAsync
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : System.Address;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : driver_types_h.cudaMemcpyKind;
      arg8 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2909
   pragma Import (C, cudaMemcpy2DAsync, "cudaMemcpy2DAsync");

   function cudaMemcpy2DToArrayAsync
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : stddef_h.size_t;
      arg4 : System.Address;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : stddef_h.size_t;
      arg8 : driver_types_h.cudaMemcpyKind;
      arg9 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:2959
   pragma Import (C, cudaMemcpy2DToArrayAsync, "cudaMemcpy2DToArrayAsync");

   function cudaMemcpy2DFromArrayAsync
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : System.Address;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : stddef_h.size_t;
      arg8 : driver_types_h.cudaMemcpyKind;
      arg9 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3009
   pragma Import (C, cudaMemcpy2DFromArrayAsync, "cudaMemcpy2DFromArrayAsync");

   function cudaMemcpyToSymbolAsync
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : driver_types_h.cudaMemcpyKind;
      arg6 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3053
   pragma Import (C, cudaMemcpyToSymbolAsync, "cudaMemcpyToSymbolAsync");

   function cudaMemcpyFromSymbolAsync
     (arg1 : System.Address;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : driver_types_h.cudaMemcpyKind;
      arg6 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3096
   pragma Import (C, cudaMemcpyFromSymbolAsync, "cudaMemcpyFromSymbolAsync");

   function cudaMemset
     (arg1 : System.Address;
      arg2 : int;
      arg3 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3118
   pragma Import (C, cudaMemset, "cudaMemset");

   function cudaMemset2D
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : int;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3144
   pragma Import (C, cudaMemset2D, "cudaMemset2D");

   function cudaMemset3D
     (arg1 : driver_types_h.cudaPitchedPtr;
      arg2 : int;
      arg3 : driver_types_h.cudaExtent) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3183
   pragma Import (C, cudaMemset3D, "cudaMemset3D");

   function cudaMemsetAsync
     (arg1 : System.Address;
      arg2 : int;
      arg3 : stddef_h.size_t;
      arg4 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3210
   pragma Import (C, cudaMemsetAsync, "cudaMemsetAsync");

   function cudaMemset2DAsync
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : int;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3242
   pragma Import (C, cudaMemset2DAsync, "cudaMemset2DAsync");

   function cudaMemset3DAsync
     (arg1 : driver_types_h.cudaPitchedPtr;
      arg2 : int;
      arg3 : driver_types_h.cudaExtent;
      arg4 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3287
   pragma Import (C, cudaMemset3DAsync, "cudaMemset3DAsync");

   function cudaGetSymbolAddress (arg1 : System.Address; arg2 : Interfaces.C.Strings.chars_ptr) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3314
   pragma Import (C, cudaGetSymbolAddress, "cudaGetSymbolAddress");

   function cudaGetSymbolSize (arg1 : access stddef_h.size_t; arg2 : Interfaces.C.Strings.chars_ptr) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3337
   pragma Import (C, cudaGetSymbolSize, "cudaGetSymbolSize");

   function cudaPointerGetAttributes (arg1 : access driver_types_h.cudaPointerAttributes; arg2 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3482
   pragma Import (C, cudaPointerGetAttributes, "cudaPointerGetAttributes");

   function cudaDeviceCanAccessPeer
     (arg1 : access int;
      arg2 : int;
      arg3 : int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3516
   pragma Import (C, cudaDeviceCanAccessPeer, "cudaDeviceCanAccessPeer");

   function cudaDeviceEnablePeerAccess (arg1 : int; arg2 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3557
   pragma Import (C, cudaDeviceEnablePeerAccess, "cudaDeviceEnablePeerAccess");

   function cudaDeviceDisablePeerAccess (arg1 : int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3582
   pragma Import (C, cudaDeviceDisablePeerAccess, "cudaDeviceDisablePeerAccess");

   function cudaGraphicsUnregisterResource (arg1 : driver_types_h.cudaGraphicsResource_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3628
   pragma Import (C, cudaGraphicsUnregisterResource, "cudaGraphicsUnregisterResource");

   function cudaGraphicsResourceSetMapFlags (arg1 : driver_types_h.cudaGraphicsResource_t; arg2 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3660
   pragma Import (C, cudaGraphicsResourceSetMapFlags, "cudaGraphicsResourceSetMapFlags");

   function cudaGraphicsMapResources
     (arg1 : int;
      arg2 : System.Address;
      arg3 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3695
   pragma Import (C, cudaGraphicsMapResources, "cudaGraphicsMapResources");

   function cudaGraphicsUnmapResources
     (arg1 : int;
      arg2 : System.Address;
      arg3 : driver_types_h.cudaStream_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3726
   pragma Import (C, cudaGraphicsUnmapResources, "cudaGraphicsUnmapResources");

   function cudaGraphicsResourceGetMappedPointer
     (arg1 : System.Address;
      arg2 : access stddef_h.size_t;
      arg3 : driver_types_h.cudaGraphicsResource_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3755
   pragma Import (C, cudaGraphicsResourceGetMappedPointer, "cudaGraphicsResourceGetMappedPointer");

   function cudaGraphicsSubResourceGetMappedArray
     (arg1 : System.Address;
      arg2 : driver_types_h.cudaGraphicsResource_t;
      arg3 : unsigned;
      arg4 : unsigned) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3789
   pragma Import (C, cudaGraphicsSubResourceGetMappedArray, "cudaGraphicsSubResourceGetMappedArray");

   function cudaGetChannelDesc (arg1 : access driver_types_h.cudaChannelFormatDesc; arg2 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3822
   pragma Import (C, cudaGetChannelDesc, "cudaGetChannelDesc");

   function cudaCreateChannelDesc
     (arg1 : int;
      arg2 : int;
      arg3 : int;
      arg4 : int;
      arg5 : driver_types_h.cudaChannelFormatKind) return driver_types_h.cudaChannelFormatDesc;  -- cuda_runtime_api.h:3857
   pragma Import (C, cudaCreateChannelDesc, "cudaCreateChannelDesc");

   function cudaBindTexture
     (arg1 : access stddef_h.size_t;
      arg2 : System.Address;
      arg3 : System.Address;
      arg4 : System.Address;
      arg5 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3899
   pragma Import (C, cudaBindTexture, "cudaBindTexture");

   function cudaBindTexture2D
     (arg1 : access stddef_h.size_t;
      arg2 : System.Address;
      arg3 : System.Address;
      arg4 : System.Address;
      arg5 : stddef_h.size_t;
      arg6 : stddef_h.size_t;
      arg7 : stddef_h.size_t) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3942
   pragma Import (C, cudaBindTexture2D, "cudaBindTexture2D");

   function cudaBindTextureToArray
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3970
   pragma Import (C, cudaBindTextureToArray, "cudaBindTextureToArray");

   function cudaUnbindTexture (arg1 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:3991
   pragma Import (C, cudaUnbindTexture, "cudaUnbindTexture");

   function cudaGetTextureAlignmentOffset (arg1 : access stddef_h.size_t; arg2 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:4016
   pragma Import (C, cudaGetTextureAlignmentOffset, "cudaGetTextureAlignmentOffset");

   function cudaGetTextureReference (arg1 : System.Address; arg2 : Interfaces.C.Strings.chars_ptr) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:4040
   pragma Import (C, cudaGetTextureReference, "cudaGetTextureReference");

   function cudaBindSurfaceToArray
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:4073
   pragma Import (C, cudaBindSurfaceToArray, "cudaBindSurfaceToArray");

   function cudaGetSurfaceReference (arg1 : System.Address; arg2 : Interfaces.C.Strings.chars_ptr) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:4091
   pragma Import (C, cudaGetSurfaceReference, "cudaGetSurfaceReference");

   function cudaDriverGetVersion (arg1 : access int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:4118
   pragma Import (C, cudaDriverGetVersion, "cudaDriverGetVersion");

   function cudaRuntimeGetVersion (arg1 : access int) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:4135
   pragma Import (C, cudaRuntimeGetVersion, "cudaRuntimeGetVersion");

   function cudaGetExportTable (arg1 : System.Address; arg2 : System.Address) return driver_types_h.cudaError_t;  -- cuda_runtime_api.h:4140
   pragma Import (C, cudaGetExportTable, "cudaGetExportTable");

end cuda_runtime_api_h;
