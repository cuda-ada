with Interfaces.C; use Interfaces.C;

package stddef_h is

   --  unsupported macro: NULL ((void *)0)
   --  arg-macro: procedure offsetof (TYPE, MEMBER)
   --    __builtin_offsetof (TYPE, MEMBER)
   subtype ptrdiff_t is int;  -- /opt/gnat-2011-i686-gnu-linux-libc2.3-bin/bin/../lib/gcc/i686-pc-linux-gnu/4.5.3/include/stddef.h:149

   subtype size_t is unsigned;  -- /opt/gnat-2011-i686-gnu-linux-libc2.3-bin/bin/../lib/gcc/i686-pc-linux-gnu/4.5.3/include/stddef.h:211

   subtype wchar_t is long;  -- /opt/gnat-2011-i686-gnu-linux-libc2.3-bin/bin/../lib/gcc/i686-pc-linux-gnu/4.5.3/include/stddef.h:323

end stddef_h;
