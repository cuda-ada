with Interfaces.C; use Interfaces.C;

package stddef_h is

   --  unsupported macro: NULL ((void *)0)
   subtype size_t is unsigned_long;  -- /opt/gnat-2011-x86_64-pc-linux-gnu-bin/bin/../lib/gcc/x86_64-pc-linux-gnu/4.5.3/include/stddef.h:211

   subtype wchar_t is int;  -- /opt/gnat-2011-x86_64-pc-linux-gnu-bin/bin/../lib/gcc/x86_64-pc-linux-gnu/4.5.3/include/stddef.h:323

end stddef_h;
