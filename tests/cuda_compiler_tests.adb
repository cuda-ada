--
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Numerics.Real_Arrays;

with CUDA.Compiler;

package body Cuda_Compiler_Tests is

   use Ahven;
   use CUDA;

   -------------------------------------------------------------------------

   procedure Compile_And_Call_Simple_Kernel
   is
      package Real_Vector_Args is new CUDA.Compiler.Arg_Creators
        (Data_Type => Ada.Numerics.Real_Arrays.Real_Vector);

      use Real_Vector_Args;

      N : constant := 32 * 1024;

      A      : Ada.Numerics.Real_Arrays.Real_Vector (1 .. N)
        := (others => 2.0);
      B      : Ada.Numerics.Real_Arrays.Real_Vector (1 .. N)
        := (others => 2.0);
      C      : aliased Ada.Numerics.Real_Arrays.Real_Vector := (1 .. N => 0.0);
      D      : aliased Ada.Numerics.Real_Arrays.Real_Vector := (1 .. N => 1.0);
      Src    : Compiler.Source_Module_Type;
      Func   : Compiler.Function_Type;
      Module : Compiler.Module_Type;
   begin
      Src := Compiler.Create
        (Preamble  => "#define N" & N'Img,
         Operation =>
         "__global__ void add( float *a, float *b, float *c, float *d ) {" &
         "   int tid = blockIdx.x;"                                        &
         "   while (tid < N) {"                                            &
         "        c[tid] = a[tid] + b[tid];"                               &
         "        d[tid] = d[tid] + a[tid];"                               &
         "        tid += gridDim.x;"                                       &
         "}}");
      Module := Compiler.Compile (Source => Src);
      Func   := Compiler.Get_Function (Module => Module,
                                       Name   => "add");

      Func.Call
        (Args =>
           (1 => In_Arg (Data => A),
            2 => In_Arg (Data => B),
            3 => Out_Arg (Data => C'Access),
            4 => In_Out_Arg (Data => D'Access)));

      for I in C'Range loop
         Assert (Condition => C (I) = A (I) + B (I),
                 Message   => "Results mismatch");
      end loop;

      for I in D'Range loop
         Assert (Condition => D (I) = A (I) + 1.0,
                 Message   => "In-out argument results mismatch");
      end loop;
   end Compile_And_Call_Simple_Kernel;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Compiler package");
      T.Add_Test_Routine
        (Routine => Source_Module_Creation'Access,
         Name    => "Create CUDA source modules");
      T.Add_Test_Routine
        (Routine => Compile_And_Call_Simple_Kernel'Access,
         Name    => "Compile and call simple kernel");
      T.Add_Test_Routine
        (Routine => Kernel_Float_Args'Access,
         Name    => "Kernel float argument passing");
      T.Add_Test_Routine
        (Routine => Kernel_Matrix_Args'Access,
         Name    => "Kernel matrix argument passing");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Kernel_Float_Args
   is
      package Float_Args is new CUDA.Compiler.Arg_Creators
        (Data_Type => Float);

      use Float_Args;

      A      : constant Float := 2.0;
      B      : constant Float := 2.0;
      C      : aliased Float;
      D      : aliased Float  := 1.0;
      Src    : Compiler.Source_Module_Type;
      Func   : Compiler.Function_Type;
      Module : Compiler.Module_Type;
   begin
      Src := Compiler.Create
        (Operation =>
         "__global__ void add( float *a, float *b, float *c, float *d) {" &
         "   *c = *a + *b;"                                               &
         "   *d = *d + *a;"                                               &
         "}");
      Module := Compiler.Compile (Source => Src);
      Func   := Compiler.Get_Function (Module => Module,
                                       Name   => "add");

      Func.Call
        (Args       =>
           (1 => In_Arg (Data => A),
            2 => In_Arg (Data => B),
            3 => Out_Arg (Data => C'Access),
            4 => In_Out_Arg (Data => D'Access)),
         Grid_Dim_X => 1);

      Assert (Condition => C = A + B,
              Message   => "Results mismatch");

      Assert (Condition => D = A + 1.0,
              Message   => "In-out argument results mismatch");
   end Kernel_Float_Args;

   -------------------------------------------------------------------------

   procedure Kernel_Matrix_Args
   is
      package Real_Matrix_Args is new CUDA.Compiler.Arg_Creators
        (Data_Type => Ada.Numerics.Real_Arrays.Real_Matrix);

      use Real_Matrix_Args;

      N         : constant := 512;
      Blocksize : constant := 8;
      Gridsize  : constant Positive
        := Positive (Float'Ceiling (Float (N) / Float (Blocksize)));

      A      : constant Ada.Numerics.Real_Arrays.Real_Matrix (1 .. N, 1 .. N)
        := (others => (others => 2.0));
      B      : constant Ada.Numerics.Real_Arrays.Real_Matrix (1 .. N, 1 .. N)
        := (others => (others => 2.0));
      C      : aliased Ada.Numerics.Real_Arrays.Real_Matrix
        := (1 .. N => (1 .. N => 0.0));
      D      : aliased Ada.Numerics.Real_Arrays.Real_Matrix
        := (1 .. N => (1 .. N => 1.0));
      Src    : Compiler.Source_Module_Type;
      Func   : Compiler.Function_Type;
      Module : Compiler.Module_Type;
   begin
      Src := Compiler.Create
        (Preamble  => "#define N" & N'Img,
         Operation =>
         "__global__ void add( float *a, float *b, float *c, float *d ) {" &
         "   int i = blockIdx.x * blockDim.x + threadIdx.x;"               &
         "   int j = blockIdx.y * blockDim.y + threadIdx.y;"               &
         "   int idx = i + j * N;"                                         &
         "   if (i < N && j < N) {"                                        &
         "      c[idx] = a[idx] + b[idx];"                                 &
         "      d[idx] = d[idx] + a[idx];"                                 &
         "   }"                                                            &
         "}");
      Module := Compiler.Compile (Source => Src);
      Func   := Compiler.Get_Function (Module => Module,
                                       Name   => "add");

      Func.Call
        (Args =>
           (1 => In_Arg (Data => A),
            2 => In_Arg (Data => B),
            3 => Out_Arg (Data => C'Access),
            4 => In_Out_Arg (Data => D'Access)),
         Grid_Dim_X  => Gridsize,
         Grid_Dim_Y  => Gridsize,
         Block_Dim_X => Blocksize,
         Block_Dim_Y => Blocksize);

      for I in C'Range (1) loop
         for J in C'Range (2) loop
            Assert (Condition => C (I, J) = A (I, J) + B (I, J),
                    Message   => "Element mismatch ("
                    & I'Img & "," & J'Img & " )");
         end loop;
      end loop;

      for I in D'Range (1) loop
         for J in D'Range (2) loop
            Assert (Condition => D (I, J) = A (I, J) + 1.0,
                    Message   => "In-out element mismatch ("
                    & I'Img & "," & J'Img & " )");
         end loop;
      end loop;
   end Kernel_Matrix_Args;

   -------------------------------------------------------------------------

   procedure Source_Module_Creation
   is
      M1, M2 : Compiler.Source_Module_Type;
      R1     : constant String := "#define V1 2345" & ASCII.LF
        & "extern ""C"" <your kernel here>";
      R2     : constant String := ASCII.LF & "extern ""C"" <your kernel here>";
   begin
      M1 := Compiler.Create (Preamble  => "#define V1 2345",
                             Operation => "<your kernel here>");
      Assert (Condition => Compiler.Get_Code (Src_Module => M1) = R1,
              Message   => "Code mismatch (1)");

      M2 := Compiler.Create (Operation => "<your kernel here>");
      Assert (Condition => Compiler.Get_Code (Src_Module => M2) = R2,
              Message   => "Code mismatch (2)");
   end Source_Module_Creation;

end Cuda_Compiler_Tests;
