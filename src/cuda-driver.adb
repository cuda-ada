--
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Fixed;

with cuda_runtime_api_h;

package body CUDA.Driver is

   package IC renames Interfaces.C;

   -------------------------------------------------------------------------

   function Clock_Rate (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.clockRate);
   end Clock_Rate;

   -------------------------------------------------------------------------

   function Compute_Capability (Dev : Device_Type) return Float
   is
   begin
      return Float'Value
        (Dev.CU_Props.major'Img & "."
         & Ada.Strings.Fixed.Trim
           (Source => Dev.CU_Props.minor'Img,
            Side   => Ada.Strings.Left));
   end Compute_Capability;

   -------------------------------------------------------------------------

   function Copy_Overlap (Dev : Device_Type) return Boolean
   is
      use type IC.int;
   begin
      return Dev.CU_Props.deviceOverlap = 1;
   end Copy_Overlap;

   -------------------------------------------------------------------------

   function Device_Count return Natural
   is
      Dev_Count : aliased IC.int;
   begin
      Check_Result (Code => cuda_runtime_api_h.cudaGetDeviceCount
                    (arg1 => Dev_Count'Access),
                    Msg  => "Unable to get device count");
      return Natural (Dev_Count);
   end Device_Count;

   -------------------------------------------------------------------------

   function Get_Device return Device_Type
   is
      Dev_Nr : aliased IC.int;
      Device : Device_Type := (others => <>);
   begin
      Check_Result (Code => cuda_runtime_api_h.cudaGetDevice
                    (arg1 => Dev_Nr'Access),
                    Msg  => "No CUDA device found");
      Check_Result (Code => cuda_runtime_api_h.cudaGetDeviceProperties
                    (arg1 => Device.CU_Props'Access,
                     arg2 => Dev_Nr),
                    Msg  => "Unable to get properties of device" & Dev_Nr'Img);

      Device.Device_Nr := Dev_Nr;
      return Device;
   end Get_Device;

   -------------------------------------------------------------------------

   procedure Iterate (Process : not null access procedure (Dev : Device_Type))
   is
      use type IC.int;

      Dev_Count : aliased IC.int;
   begin
      Check_Result (Code => cuda_runtime_api_h.cudaGetDeviceCount
                    (arg1 => Dev_Count'Access),
                    Msg  => "Unable to get device count");

      for D in 0 .. Dev_Count - 1 loop
         declare
            Device : Device_Type := (Device_Nr => D, others => <>);
         begin
            Check_Result
              (Code => cuda_runtime_api_h.cudaGetDeviceProperties
                 (arg1 => Device.CU_Props'Access,
                  arg2 => D),
               Msg  => "Unable to get properties of device" & D'Img);

            Process (Dev => Device);
         end;
      end loop;
   end Iterate;

   -------------------------------------------------------------------------

   function Kernel_Exec_Timeout (Dev : Device_Type) return Boolean
   is
      use type IC.int;
   begin
      return Dev.CU_Props.kernelExecTimeoutEnabled = 1;
   end Kernel_Exec_Timeout;

   -------------------------------------------------------------------------

   function Max_Grid_Size (Dev : Device_Type) return Nat_3_Array
   is
   begin
      return (1 => Natural (Dev.CU_Props.maxGridSize (0)),
              2 => Natural (Dev.CU_Props.maxGridSize (1)),
              3 => Natural (Dev.CU_Props.maxGridSize (2)));
   end Max_Grid_Size;

   -------------------------------------------------------------------------

   function Max_Mem_Pitch (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.memPitch);
   end Max_Mem_Pitch;

   -------------------------------------------------------------------------

   function Max_Thread_Dimensions (Dev : Device_Type) return Nat_3_Array
   is
   begin
      return (1 => Natural (Dev.CU_Props.maxThreadsDim (0)),
              2 => Natural (Dev.CU_Props.maxThreadsDim (1)),
              3 => Natural (Dev.CU_Props.maxThreadsDim (2)));
   end Max_Thread_Dimensions;

   -------------------------------------------------------------------------

   function Max_Threads_Per_Block (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.maxThreadsPerBlock);
   end Max_Threads_Per_Block;

   -------------------------------------------------------------------------

   function Multiprocessor_Count (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.multiProcessorCount);
   end Multiprocessor_Count;

   -------------------------------------------------------------------------

   function Name (Dev : Device_Type) return String
   is
   begin
      return IC.To_Ada (Dev.CU_Props.name);
   end Name;

   -------------------------------------------------------------------------

   function Regs_Per_Block (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.regsPerBlock);
   end Regs_Per_Block;

   -------------------------------------------------------------------------

   function Shared_Mem_Per_Block (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.sharedMemPerBlock);
   end Shared_Mem_Per_Block;

   -------------------------------------------------------------------------

   function Texture_Alignment (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.textureAlignment);
   end Texture_Alignment;

   -------------------------------------------------------------------------

   function Total_Constant_Mem (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.totalConstMem);
   end Total_Constant_Mem;

   -------------------------------------------------------------------------

   function Total_Global_Mem (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.totalGlobalMem);
   end Total_Global_Mem;

   -------------------------------------------------------------------------

   function Warp_Size (Dev : Device_Type) return Natural
   is
   begin
      return Natural (Dev.CU_Props.warpSize);
   end Warp_Size;

end CUDA.Driver;
