/*
 *  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
 *  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *  University of Applied Sciences Rapperswil
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  This software contains source code provided by NVIDIA Corporation.
 *  The CUDA kernel for the matrix multiplication is based on the source
 *  provided in NVIDIA SDK code sample, see:
 *    http://developer.nvidia.com/cuda-cc-sdk-code-samples#matrixMul
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <cuda.h>

#define N          512
#define BLOCK_SIZE 16

__global__ void mul(float* A, float* B, float* C)
{
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int aBegin = N * BLOCK_SIZE * by;
	int aEnd   = aBegin + N - 1;
	int aStep  = BLOCK_SIZE;
	int bBegin = BLOCK_SIZE * bx;
	int bStep  = BLOCK_SIZE * N;
	float Csub = 0;
	for (int a = aBegin, b = bBegin;
			a <= aEnd;
			a += aStep, b += bStep) {
		__shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
		As[ty][tx] = A[a + N * ty + tx];
		Bs[ty][tx] = B[b + N * ty + tx];
		__syncthreads();
		for (int k = 0; k < BLOCK_SIZE; ++k)
			Csub += As[ty][k] * Bs[k][tx];
		__syncthreads();
	}
	int c = N * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + N * ty + tx] = Csub;
}

void check_result(cudaError code, const char* message)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "%s (%d)\n", message, (int) code);
		exit(-1);
	}
}

void gpu_mul(float* A, float* B, float* C, dim3 bsize, dim3 gsize)
{
	float *A_Dev, *B_Dev, *C_Dev;
	const size_t mem_size = sizeof(float) * N * N;

	check_result(cudaMalloc((void**) &A_Dev, mem_size),
			"Unable to allocate device memory");
	check_result(cudaMalloc((void**) &B_Dev, mem_size),
			"Unable to allocate device memory");
	check_result(cudaMalloc((void**) &C_Dev, mem_size),
			"Unable to allocate device memory");

	check_result(cudaMemcpy(A_Dev, A, mem_size, cudaMemcpyHostToDevice),
			"Unable to copy variable to device");
	check_result(cudaMemcpy(B_Dev, B, mem_size, cudaMemcpyHostToDevice),
			"Unable to copy variable to device");

	mul<<<gsize, bsize>>>(A_Dev, B_Dev, C_Dev);

	check_result(cudaMemcpy(C, C_Dev, mem_size, cudaMemcpyDeviceToHost),
			"Unable to copy output variable from device");

	check_result(cudaFree(A_Dev), "Error freeing device memory");
	check_result(cudaFree(B_Dev), "Error freeing device memory");
	check_result(cudaFree(C_Dev), "Error freeing device memory");
}

int main(int argc, char** argv)
{
	const unsigned int mem_size = sizeof(float) * N * N;
	int i, count = 1;
	float *A, *B, *C;
	double elapsed_time = 0;
	timeval t1, t2;

	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(ceil((float)(N) / (float)(BLOCK_SIZE)),
			ceil((float)(N) / (float)(BLOCK_SIZE)));

	printf("CUDA Runtime API reference performance measurement\n");

	if (argc == 2) {
		if ((count = atoi(argv[1])) < 1) {
			fprintf(stderr, "Invalid iteration count\n");
			return -1;
		}
	}

	A = (float*)malloc(mem_size);
	B = (float*)malloc(mem_size);
	C = (float*)malloc(mem_size);

	for (i = 0; i < N * N; ++i) {
		A[i] = B[i] = 2.0;
		C[i] = 0.0;
	}

	printf("Executing %d iteration(s)\n", count);
	printf("- Matrix [1 .. %u, 1 .. %u]\n", N, N);
	printf("- Grid size %u\n", dimGrid.x);
	printf("- Block size %u\n", dimBlock.x);

	for (i = 0; i < count; i++) {
		gettimeofday(&t1, NULL);
		gpu_mul(A, B, C, dimBlock, dimGrid);
		gettimeofday(&t2, NULL);
		elapsed_time = elapsed_time +
			((t2.tv_sec - t1.tv_sec) * 1000.0) +
			((t2.tv_usec - t1.tv_usec) / 1000.0);
	}

	printf("Total CUDA Runtime API execution time  : %.9f\n",
			elapsed_time / 1000.0);

	free(A);
	free(B);
	free(C);

	return 0;
}
