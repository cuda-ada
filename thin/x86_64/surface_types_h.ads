with Interfaces.C; use Interfaces.C;
with driver_types_h;

package surface_types_h is

   type cudaSurfaceBoundaryMode is 
     (cudaBoundaryModeZero,
      cudaBoundaryModeClamp,
      cudaBoundaryModeTrap);
   pragma Convention (C, cudaSurfaceBoundaryMode);  -- surface_types.h:77

   type cudaSurfaceFormatMode is 
     (cudaFormatModeForced,
      cudaFormatModeAuto);
   pragma Convention (C, cudaSurfaceFormatMode);  -- surface_types.h:88

   type surfaceReference is record
      channelDesc : aliased driver_types_h.cudaChannelFormatDesc;  -- surface_types.h:103
   end record;
   pragma Convention (C_Pass_By_Copy, surfaceReference);  -- surface_types.h:98

end surface_types_h;
