with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;

package vector_types_h is

   type char1 is record
      x : aliased signed_char;  -- vector_types.h:94
   end record;
   pragma Convention (C_Pass_By_Copy, char1);  -- vector_types.h:92

   type uchar1 is record
      x : aliased unsigned_char;  -- vector_types.h:100
   end record;
   pragma Convention (C_Pass_By_Copy, uchar1);  -- vector_types.h:98

   type char2 is record
      x : aliased signed_char;  -- vector_types.h:106
      y : aliased signed_char;  -- vector_types.h:106
   end record;
   pragma Convention (C_Pass_By_Copy, char2);  -- vector_types.h:104

   type uchar2 is record
      x : aliased unsigned_char;  -- vector_types.h:112
      y : aliased unsigned_char;  -- vector_types.h:112
   end record;
   pragma Convention (C_Pass_By_Copy, uchar2);  -- vector_types.h:110

   type char3 is record
      x : aliased signed_char;  -- vector_types.h:118
      y : aliased signed_char;  -- vector_types.h:118
      z : aliased signed_char;  -- vector_types.h:118
   end record;
   pragma Convention (C_Pass_By_Copy, char3);  -- vector_types.h:116

   type uchar3 is record
      x : aliased unsigned_char;  -- vector_types.h:124
      y : aliased unsigned_char;  -- vector_types.h:124
      z : aliased unsigned_char;  -- vector_types.h:124
   end record;
   pragma Convention (C_Pass_By_Copy, uchar3);  -- vector_types.h:122

   type char4 is record
      x : aliased signed_char;  -- vector_types.h:130
      y : aliased signed_char;  -- vector_types.h:130
      z : aliased signed_char;  -- vector_types.h:130
      w : aliased signed_char;  -- vector_types.h:130
   end record;
   pragma Convention (C_Pass_By_Copy, char4);  -- vector_types.h:128

   type uchar4 is record
      x : aliased unsigned_char;  -- vector_types.h:136
      y : aliased unsigned_char;  -- vector_types.h:136
      z : aliased unsigned_char;  -- vector_types.h:136
      w : aliased unsigned_char;  -- vector_types.h:136
   end record;
   pragma Convention (C_Pass_By_Copy, uchar4);  -- vector_types.h:134

   type short1 is record
      x : aliased short;  -- vector_types.h:142
   end record;
   pragma Convention (C_Pass_By_Copy, short1);  -- vector_types.h:140

   type ushort1 is record
      x : aliased unsigned_short;  -- vector_types.h:148
   end record;
   pragma Convention (C_Pass_By_Copy, ushort1);  -- vector_types.h:146

   type short2 is record
      x : aliased short;  -- vector_types.h:154
      y : aliased short;  -- vector_types.h:154
   end record;
   pragma Convention (C_Pass_By_Copy, short2);  -- vector_types.h:152

   type ushort2 is record
      x : aliased unsigned_short;  -- vector_types.h:160
      y : aliased unsigned_short;  -- vector_types.h:160
   end record;
   pragma Convention (C_Pass_By_Copy, ushort2);  -- vector_types.h:158

   type short3 is record
      x : aliased short;  -- vector_types.h:166
      y : aliased short;  -- vector_types.h:166
      z : aliased short;  -- vector_types.h:166
   end record;
   pragma Convention (C_Pass_By_Copy, short3);  -- vector_types.h:164

   type ushort3 is record
      x : aliased unsigned_short;  -- vector_types.h:172
      y : aliased unsigned_short;  -- vector_types.h:172
      z : aliased unsigned_short;  -- vector_types.h:172
   end record;
   pragma Convention (C_Pass_By_Copy, ushort3);  -- vector_types.h:170

   type short4 is record
      x : aliased short;  -- vector_types.h:176
      y : aliased short;  -- vector_types.h:176
      z : aliased short;  -- vector_types.h:176
      w : aliased short;  -- vector_types.h:176
   end record;
   pragma Convention (C_Pass_By_Copy, short4);  -- vector_types.h:176

   type ushort4 is record
      x : aliased unsigned_short;  -- vector_types.h:179
      y : aliased unsigned_short;  -- vector_types.h:179
      z : aliased unsigned_short;  -- vector_types.h:179
      w : aliased unsigned_short;  -- vector_types.h:179
   end record;
   pragma Convention (C_Pass_By_Copy, ushort4);  -- vector_types.h:179

   type int1 is record
      x : aliased int;  -- vector_types.h:184
   end record;
   pragma Convention (C_Pass_By_Copy, int1);  -- vector_types.h:182

   type uint1 is record
      x : aliased unsigned;  -- vector_types.h:190
   end record;
   pragma Convention (C_Pass_By_Copy, uint1);  -- vector_types.h:188

   type int2 is record
      x : aliased int;  -- vector_types.h:194
      y : aliased int;  -- vector_types.h:194
   end record;
   pragma Convention (C_Pass_By_Copy, int2);  -- vector_types.h:194

   type uint2 is record
      x : aliased unsigned;  -- vector_types.h:197
      y : aliased unsigned;  -- vector_types.h:197
   end record;
   pragma Convention (C_Pass_By_Copy, uint2);  -- vector_types.h:197

   type int3 is record
      x : aliased int;  -- vector_types.h:202
      y : aliased int;  -- vector_types.h:202
      z : aliased int;  -- vector_types.h:202
   end record;
   pragma Convention (C_Pass_By_Copy, int3);  -- vector_types.h:200

   type uint3 is record
      x : aliased unsigned;  -- vector_types.h:208
      y : aliased unsigned;  -- vector_types.h:208
      z : aliased unsigned;  -- vector_types.h:208
   end record;
   pragma Convention (C_Pass_By_Copy, uint3);  -- vector_types.h:206

   type int4 is record
      x : aliased int;  -- vector_types.h:214
      y : aliased int;  -- vector_types.h:214
      z : aliased int;  -- vector_types.h:214
      w : aliased int;  -- vector_types.h:214
   end record;
   pragma Convention (C_Pass_By_Copy, int4);  -- vector_types.h:212

   type uint4 is record
      x : aliased unsigned;  -- vector_types.h:220
      y : aliased unsigned;  -- vector_types.h:220
      z : aliased unsigned;  -- vector_types.h:220
      w : aliased unsigned;  -- vector_types.h:220
   end record;
   pragma Convention (C_Pass_By_Copy, uint4);  -- vector_types.h:218

   type long1 is record
      x : aliased long;  -- vector_types.h:226
   end record;
   pragma Convention (C_Pass_By_Copy, long1);  -- vector_types.h:224

   type ulong1 is record
      x : aliased unsigned_long;  -- vector_types.h:232
   end record;
   pragma Convention (C_Pass_By_Copy, ulong1);  -- vector_types.h:230

   type long2 is record
      x : aliased long;  -- vector_types.h:248
      y : aliased long;  -- vector_types.h:248
   end record;
   pragma Convention (C_Pass_By_Copy, long2);  -- vector_types.h:246

   type ulong2 is record
      x : aliased unsigned_long;  -- vector_types.h:254
      y : aliased unsigned_long;  -- vector_types.h:254
   end record;
   pragma Convention (C_Pass_By_Copy, ulong2);  -- vector_types.h:252

   type long3 is record
      x : aliased long;  -- vector_types.h:262
      y : aliased long;  -- vector_types.h:262
      z : aliased long;  -- vector_types.h:262
   end record;
   pragma Convention (C_Pass_By_Copy, long3);  -- vector_types.h:260

   type ulong3 is record
      x : aliased unsigned_long;  -- vector_types.h:268
      y : aliased unsigned_long;  -- vector_types.h:268
      z : aliased unsigned_long;  -- vector_types.h:268
   end record;
   pragma Convention (C_Pass_By_Copy, ulong3);  -- vector_types.h:266

   type long4 is record
      x : aliased long;  -- vector_types.h:274
      y : aliased long;  -- vector_types.h:274
      z : aliased long;  -- vector_types.h:274
      w : aliased long;  -- vector_types.h:274
   end record;
   pragma Convention (C_Pass_By_Copy, long4);  -- vector_types.h:272

   type ulong4 is record
      x : aliased unsigned_long;  -- vector_types.h:280
      y : aliased unsigned_long;  -- vector_types.h:280
      z : aliased unsigned_long;  -- vector_types.h:280
      w : aliased unsigned_long;  -- vector_types.h:280
   end record;
   pragma Convention (C_Pass_By_Copy, ulong4);  -- vector_types.h:278

   type float1 is record
      x : aliased float;  -- vector_types.h:286
   end record;
   pragma Convention (C_Pass_By_Copy, float1);  -- vector_types.h:284

   type float2 is record
      x : aliased float;  -- vector_types.h:290
      y : aliased float;  -- vector_types.h:290
   end record;
   pragma Convention (C_Pass_By_Copy, float2);  -- vector_types.h:290

   type float3 is record
      x : aliased float;  -- vector_types.h:295
      y : aliased float;  -- vector_types.h:295
      z : aliased float;  -- vector_types.h:295
   end record;
   pragma Convention (C_Pass_By_Copy, float3);  -- vector_types.h:293

   type float4 is record
      x : aliased float;  -- vector_types.h:301
      y : aliased float;  -- vector_types.h:301
      z : aliased float;  -- vector_types.h:301
      w : aliased float;  -- vector_types.h:301
   end record;
   pragma Convention (C_Pass_By_Copy, float4);  -- vector_types.h:299

   type longlong1 is record
      x : aliased Long_Long_Integer;  -- vector_types.h:307
   end record;
   pragma Convention (C_Pass_By_Copy, longlong1);  -- vector_types.h:305

   type ulonglong1 is record
      x : aliased Extensions.unsigned_long_long;  -- vector_types.h:313
   end record;
   pragma Convention (C_Pass_By_Copy, ulonglong1);  -- vector_types.h:311

   type longlong2 is record
      x : aliased Long_Long_Integer;  -- vector_types.h:319
      y : aliased Long_Long_Integer;  -- vector_types.h:319
   end record;
   pragma Convention (C_Pass_By_Copy, longlong2);  -- vector_types.h:317

   type ulonglong2 is record
      x : aliased Extensions.unsigned_long_long;  -- vector_types.h:325
      y : aliased Extensions.unsigned_long_long;  -- vector_types.h:325
   end record;
   pragma Convention (C_Pass_By_Copy, ulonglong2);  -- vector_types.h:323

   type longlong3 is record
      x : aliased Long_Long_Integer;  -- vector_types.h:331
      y : aliased Long_Long_Integer;  -- vector_types.h:331
      z : aliased Long_Long_Integer;  -- vector_types.h:331
   end record;
   pragma Convention (C_Pass_By_Copy, longlong3);  -- vector_types.h:329

   type ulonglong3 is record
      x : aliased Extensions.unsigned_long_long;  -- vector_types.h:337
      y : aliased Extensions.unsigned_long_long;  -- vector_types.h:337
      z : aliased Extensions.unsigned_long_long;  -- vector_types.h:337
   end record;
   pragma Convention (C_Pass_By_Copy, ulonglong3);  -- vector_types.h:335

   type longlong4 is record
      x : aliased Long_Long_Integer;  -- vector_types.h:343
      y : aliased Long_Long_Integer;  -- vector_types.h:343
      z : aliased Long_Long_Integer;  -- vector_types.h:343
      w : aliased Long_Long_Integer;  -- vector_types.h:343
   end record;
   pragma Convention (C_Pass_By_Copy, longlong4);  -- vector_types.h:341

   type ulonglong4 is record
      x : aliased Extensions.unsigned_long_long;  -- vector_types.h:349
      y : aliased Extensions.unsigned_long_long;  -- vector_types.h:349
      z : aliased Extensions.unsigned_long_long;  -- vector_types.h:349
      w : aliased Extensions.unsigned_long_long;  -- vector_types.h:349
   end record;
   pragma Convention (C_Pass_By_Copy, ulonglong4);  -- vector_types.h:347

   type double1 is record
      x : aliased double;  -- vector_types.h:355
   end record;
   pragma Convention (C_Pass_By_Copy, double1);  -- vector_types.h:353

   type double2 is record
      x : aliased double;  -- vector_types.h:361
      y : aliased double;  -- vector_types.h:361
   end record;
   pragma Convention (C_Pass_By_Copy, double2);  -- vector_types.h:359

   type double3 is record
      x : aliased double;  -- vector_types.h:367
      y : aliased double;  -- vector_types.h:367
      z : aliased double;  -- vector_types.h:367
   end record;
   pragma Convention (C_Pass_By_Copy, double3);  -- vector_types.h:365

   type double4 is record
      x : aliased double;  -- vector_types.h:373
      y : aliased double;  -- vector_types.h:373
      z : aliased double;  -- vector_types.h:373
      w : aliased double;  -- vector_types.h:373
   end record;
   pragma Convention (C_Pass_By_Copy, double4);  -- vector_types.h:371

   type dim3 is record
      x : aliased unsigned;  -- vector_types.h:495
      y : aliased unsigned;  -- vector_types.h:495
      z : aliased unsigned;  -- vector_types.h:495
   end record;
   pragma Convention (C_Pass_By_Copy, dim3);  -- vector_types.h:493

end vector_types_h;
