with Interfaces.C; use Interfaces.C;
with System;
with stddef_h;
with Interfaces.C.Strings;

package cuda_h is

   --  unsupported macro: cuDeviceTotalMem cuDeviceTotalMem_v2
   --  unsupported macro: cuCtxCreate cuCtxCreate_v2
   --  unsupported macro: cuModuleGetGlobal cuModuleGetGlobal_v2
   --  unsupported macro: cuMemGetInfo cuMemGetInfo_v2
   --  unsupported macro: cuMemAlloc cuMemAlloc_v2
   --  unsupported macro: cuMemAllocPitch cuMemAllocPitch_v2
   --  unsupported macro: cuMemFree cuMemFree_v2
   --  unsupported macro: cuMemGetAddressRange cuMemGetAddressRange_v2
   --  unsupported macro: cuMemAllocHost cuMemAllocHost_v2
   --  unsupported macro: cuMemHostGetDevicePointer cuMemHostGetDevicePointer_v2
   --  unsupported macro: cuMemcpyHtoD cuMemcpyHtoD_v2
   --  unsupported macro: cuMemcpyDtoH cuMemcpyDtoH_v2
   --  unsupported macro: cuMemcpyDtoD cuMemcpyDtoD_v2
   --  unsupported macro: cuMemcpyDtoA cuMemcpyDtoA_v2
   --  unsupported macro: cuMemcpyAtoD cuMemcpyAtoD_v2
   --  unsupported macro: cuMemcpyHtoA cuMemcpyHtoA_v2
   --  unsupported macro: cuMemcpyAtoH cuMemcpyAtoH_v2
   --  unsupported macro: cuMemcpyAtoA cuMemcpyAtoA_v2
   --  unsupported macro: cuMemcpyHtoAAsync cuMemcpyHtoAAsync_v2
   --  unsupported macro: cuMemcpyAtoHAsync cuMemcpyAtoHAsync_v2
   --  unsupported macro: cuMemcpy2D cuMemcpy2D_v2
   --  unsupported macro: cuMemcpy2DUnaligned cuMemcpy2DUnaligned_v2
   --  unsupported macro: cuMemcpy3D cuMemcpy3D_v2
   --  unsupported macro: cuMemcpyHtoDAsync cuMemcpyHtoDAsync_v2
   --  unsupported macro: cuMemcpyDtoHAsync cuMemcpyDtoHAsync_v2
   --  unsupported macro: cuMemcpyDtoDAsync cuMemcpyDtoDAsync_v2
   --  unsupported macro: cuMemcpy2DAsync cuMemcpy2DAsync_v2
   --  unsupported macro: cuMemcpy3DAsync cuMemcpy3DAsync_v2
   --  unsupported macro: cuMemsetD8 cuMemsetD8_v2
   --  unsupported macro: cuMemsetD16 cuMemsetD16_v2
   --  unsupported macro: cuMemsetD32 cuMemsetD32_v2
   --  unsupported macro: cuMemsetD2D8 cuMemsetD2D8_v2
   --  unsupported macro: cuMemsetD2D16 cuMemsetD2D16_v2
   --  unsupported macro: cuMemsetD2D32 cuMemsetD2D32_v2
   --  unsupported macro: cuArrayCreate cuArrayCreate_v2
   --  unsupported macro: cuArrayGetDescriptor cuArrayGetDescriptor_v2
   --  unsupported macro: cuArray3DCreate cuArray3DCreate_v2
   --  unsupported macro: cuArray3DGetDescriptor cuArray3DGetDescriptor_v2
   --  unsupported macro: cuTexRefSetAddress cuTexRefSetAddress_v2
   --  unsupported macro: cuTexRefSetAddress2D cuTexRefSetAddress2D_v2
   --  unsupported macro: cuTexRefGetAddress cuTexRefGetAddress_v2
   --  unsupported macro: cuGraphicsResourceGetMappedPointer cuGraphicsResourceGetMappedPointer_v2
   --  unsupported macro: cuCtxDestroy cuCtxDestroy_v2
   --  unsupported macro: cuCtxPopCurrent cuCtxPopCurrent_v2
   --  unsupported macro: cuCtxPushCurrent cuCtxPushCurrent_v2
   --  unsupported macro: cuStreamDestroy cuStreamDestroy_v2
   --  unsupported macro: cuEventDestroy cuEventDestroy_v2

   CUDA_VERSION : constant := 4000;  --  cuda.h:137

   CU_MEMHOSTALLOC_PORTABLE : constant := 16#01#;  --  cuda.h:834

   CU_MEMHOSTALLOC_DEVICEMAP : constant := 16#02#;  --  cuda.h:841

   CU_MEMHOSTALLOC_WRITECOMBINED : constant := 16#04#;  --  cuda.h:849

   CU_MEMHOSTREGISTER_PORTABLE : constant := 16#01#;  --  cuda.h:855

   CU_MEMHOSTREGISTER_DEVICEMAP : constant := 16#02#;  --  cuda.h:862

   CUDA_ARRAY3D_LAYERED : constant := 16#01#;  --  cuda.h:991

   CUDA_ARRAY3D_2DARRAY : constant := 16#01#;  --  cuda.h:996

   CUDA_ARRAY3D_SURFACE_LDST : constant := 16#02#;  --  cuda.h:1002

   CU_TRSA_OVERRIDE_FORMAT : constant := 16#01#;  --  cuda.h:1008

   CU_TRSF_READ_AS_INTEGER : constant := 16#01#;  --  cuda.h:1015

   CU_TRSF_NORMALIZED_COORDINATES : constant := 16#02#;  --  cuda.h:1021

   CU_TRSF_SRGB : constant := 16#10#;  --  cuda.h:1027
   --  unsupported macro: CU_LAUNCH_PARAM_END ((void*)0x00)
   --  unsupported macro: CU_LAUNCH_PARAM_BUFFER_POINTER ((void*)0x01)
   --  unsupported macro: CU_LAUNCH_PARAM_BUFFER_SIZE ((void*)0x02)

   CU_PARAM_TR_DEFAULT : constant := -1;  --  cuda.h:1060

   subtype CUdeviceptr is unsigned;  -- cuda.h:151

   subtype CUdevice is int;  -- cuda.h:156

   --  skipped empty struct CUctx_st

   type CUcontext is new System.Address;  -- cuda.h:157

   --  skipped empty struct CUmod_st

   type CUmodule is new System.Address;  -- cuda.h:158

   --  skipped empty struct CUfunc_st

   type CUfunction is new System.Address;  -- cuda.h:159

   --  skipped empty struct CUarray_st

   type CUarray is new System.Address;  -- cuda.h:160

   --  skipped empty struct CUtexref_st

   type CUtexref is new System.Address;  -- cuda.h:161

   --  skipped empty struct CUsurfref_st

   type CUsurfref is new System.Address;  -- cuda.h:162

   --  skipped empty struct CUevent_st

   type CUevent is new System.Address;  -- cuda.h:163

   --  skipped empty struct CUstream_st

   type CUstream is new System.Address;  -- cuda.h:164

   --  skipped empty struct CUgraphicsResource_st

   type CUgraphicsResource is new System.Address;  -- cuda.h:165

   subtype anon1074_anon1076_array is Interfaces.C.char_array (0 .. 15);
   type CUuuid_st is record
      bytes : aliased anon1074_anon1076_array;  -- cuda.h:168
   end record;
   pragma Convention (C_Pass_By_Copy, CUuuid_st);  -- cuda.h:167

   subtype CUuuid is CUuuid_st;

   subtype CUctx_flags_enum is unsigned;
   CU_CTX_SCHED_AUTO : constant CUctx_flags_enum := 0;
   CU_CTX_SCHED_SPIN : constant CUctx_flags_enum := 1;
   CU_CTX_SCHED_YIELD : constant CUctx_flags_enum := 2;
   CU_CTX_SCHED_BLOCKING_SYNC : constant CUctx_flags_enum := 4;
   CU_CTX_BLOCKING_SYNC : constant CUctx_flags_enum := 4;
   CU_CTX_SCHED_MASK : constant CUctx_flags_enum := 7;
   CU_CTX_MAP_HOST : constant CUctx_flags_enum := 8;
   CU_CTX_LMEM_RESIZE_TO_MAX : constant CUctx_flags_enum := 16;
   CU_CTX_FLAGS_MASK : constant CUctx_flags_enum := 31;  -- cuda.h:174

   subtype CUctx_flags is CUctx_flags_enum;

   type CUevent_flags_enum is
     (CU_EVENT_DEFAULT,
      CU_EVENT_BLOCKING_SYNC,
      CU_EVENT_DISABLE_TIMING);
   pragma Convention (C, CUevent_flags_enum);  -- cuda.h:189

   subtype CUevent_flags is CUevent_flags_enum;

   subtype CUarray_format_enum is unsigned;
   CU_AD_FORMAT_UNSIGNED_INT8 : constant CUarray_format_enum := 1;
   CU_AD_FORMAT_UNSIGNED_INT16 : constant CUarray_format_enum := 2;
   CU_AD_FORMAT_UNSIGNED_INT32 : constant CUarray_format_enum := 3;
   CU_AD_FORMAT_SIGNED_INT8 : constant CUarray_format_enum := 8;
   CU_AD_FORMAT_SIGNED_INT16 : constant CUarray_format_enum := 9;
   CU_AD_FORMAT_SIGNED_INT32 : constant CUarray_format_enum := 10;
   CU_AD_FORMAT_HALF : constant CUarray_format_enum := 16;
   CU_AD_FORMAT_FLOAT : constant CUarray_format_enum := 32;  -- cuda.h:198

   subtype CUarray_format is CUarray_format_enum;

   type CUaddress_mode_enum is
     (CU_TR_ADDRESS_MODE_WRAP,
      CU_TR_ADDRESS_MODE_CLAMP,
      CU_TR_ADDRESS_MODE_MIRROR,
      CU_TR_ADDRESS_MODE_BORDER);
   pragma Convention (C, CUaddress_mode_enum);  -- cuda.h:212

   subtype CUaddress_mode is CUaddress_mode_enum;

   type CUfilter_mode_enum is
     (CU_TR_FILTER_MODE_POINT,
      CU_TR_FILTER_MODE_LINEAR);
   pragma Convention (C, CUfilter_mode_enum);  -- cuda.h:222

   subtype CUfilter_mode is CUfilter_mode_enum;

   subtype CUdevice_attribute_enum is unsigned;
   CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK : constant CUdevice_attribute_enum := 1;
   CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X : constant CUdevice_attribute_enum := 2;
   CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y : constant CUdevice_attribute_enum := 3;
   CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z : constant CUdevice_attribute_enum := 4;
   CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X : constant CUdevice_attribute_enum := 5;
   CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y : constant CUdevice_attribute_enum := 6;
   CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z : constant CUdevice_attribute_enum := 7;
   CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK : constant CUdevice_attribute_enum := 8;
   CU_DEVICE_ATTRIBUTE_SHARED_MEMORY_PER_BLOCK : constant CUdevice_attribute_enum := 8;
   CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY : constant CUdevice_attribute_enum := 9;
   CU_DEVICE_ATTRIBUTE_WARP_SIZE : constant CUdevice_attribute_enum := 10;
   CU_DEVICE_ATTRIBUTE_MAX_PITCH : constant CUdevice_attribute_enum := 11;
   CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK : constant CUdevice_attribute_enum := 12;
   CU_DEVICE_ATTRIBUTE_REGISTERS_PER_BLOCK : constant CUdevice_attribute_enum := 12;
   CU_DEVICE_ATTRIBUTE_CLOCK_RATE : constant CUdevice_attribute_enum := 13;
   CU_DEVICE_ATTRIBUTE_TEXTURE_ALIGNMENT : constant CUdevice_attribute_enum := 14;
   CU_DEVICE_ATTRIBUTE_GPU_OVERLAP : constant CUdevice_attribute_enum := 15;
   CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT : constant CUdevice_attribute_enum := 16;
   CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT : constant CUdevice_attribute_enum := 17;
   CU_DEVICE_ATTRIBUTE_INTEGRATED : constant CUdevice_attribute_enum := 18;
   CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY : constant CUdevice_attribute_enum := 19;
   CU_DEVICE_ATTRIBUTE_COMPUTE_MODE : constant CUdevice_attribute_enum := 20;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_WIDTH : constant CUdevice_attribute_enum := 21;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_WIDTH : constant CUdevice_attribute_enum := 22;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_HEIGHT : constant CUdevice_attribute_enum := 23;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_WIDTH : constant CUdevice_attribute_enum := 24;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_HEIGHT : constant CUdevice_attribute_enum := 25;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_DEPTH : constant CUdevice_attribute_enum := 26;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_WIDTH : constant CUdevice_attribute_enum := 27;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_HEIGHT : constant CUdevice_attribute_enum := 28;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_LAYERS : constant CUdevice_attribute_enum := 29;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_WIDTH : constant CUdevice_attribute_enum := 27;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_HEIGHT : constant CUdevice_attribute_enum := 28;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_NUMSLICES : constant CUdevice_attribute_enum := 29;
   CU_DEVICE_ATTRIBUTE_SURFACE_ALIGNMENT : constant CUdevice_attribute_enum := 30;
   CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS : constant CUdevice_attribute_enum := 31;
   CU_DEVICE_ATTRIBUTE_ECC_ENABLED : constant CUdevice_attribute_enum := 32;
   CU_DEVICE_ATTRIBUTE_PCI_BUS_ID : constant CUdevice_attribute_enum := 33;
   CU_DEVICE_ATTRIBUTE_PCI_DEVICE_ID : constant CUdevice_attribute_enum := 34;
   CU_DEVICE_ATTRIBUTE_TCC_DRIVER : constant CUdevice_attribute_enum := 35;
   CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE : constant CUdevice_attribute_enum := 36;
   CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH : constant CUdevice_attribute_enum := 37;
   CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE : constant CUdevice_attribute_enum := 38;
   CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR : constant CUdevice_attribute_enum := 39;
   CU_DEVICE_ATTRIBUTE_ASYNC_ENGINE_COUNT : constant CUdevice_attribute_enum := 40;
   CU_DEVICE_ATTRIBUTE_UNIFIED_ADDRESSING : constant CUdevice_attribute_enum := 41;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LAYERED_WIDTH : constant CUdevice_attribute_enum := 42;
   CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LAYERED_LAYERS : constant CUdevice_attribute_enum := 43;
   CU_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID : constant CUdevice_attribute_enum := 50;  -- cuda.h:230

   subtype CUdevice_attribute is CUdevice_attribute_enum;

   type anon1090_anon1092_array is array (0 .. 2) of aliased int;
   type CUdevprop_st is record
      maxThreadsPerBlock : aliased int;  -- cuda.h:286
      maxThreadsDim : aliased anon1090_anon1092_array;  -- cuda.h:287
      maxGridSize : aliased anon1090_anon1092_array;  -- cuda.h:288
      sharedMemPerBlock : aliased int;  -- cuda.h:289
      totalConstantMemory : aliased int;  -- cuda.h:290
      SIMDWidth : aliased int;  -- cuda.h:291
      memPitch : aliased int;  -- cuda.h:292
      regsPerBlock : aliased int;  -- cuda.h:293
      clockRate : aliased int;  -- cuda.h:294
      textureAlign : aliased int;  -- cuda.h:295
   end record;
   pragma Convention (C_Pass_By_Copy, CUdevprop_st);  -- cuda.h:285

   subtype CUdevprop is CUdevprop_st;

   subtype CUpointer_attribute_enum is unsigned;
   CU_POINTER_ATTRIBUTE_CONTEXT : constant CUpointer_attribute_enum := 1;
   CU_POINTER_ATTRIBUTE_MEMORY_TYPE : constant CUpointer_attribute_enum := 2;
   CU_POINTER_ATTRIBUTE_DEVICE_POINTER : constant CUpointer_attribute_enum := 3;
   CU_POINTER_ATTRIBUTE_HOST_POINTER : constant CUpointer_attribute_enum := 4;  -- cuda.h:301

   subtype CUpointer_attribute is CUpointer_attribute_enum;

   type CUfunction_attribute_enum is
     (CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK,
      CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES,
      CU_FUNC_ATTRIBUTE_CONST_SIZE_BYTES,
      CU_FUNC_ATTRIBUTE_LOCAL_SIZE_BYTES,
      CU_FUNC_ATTRIBUTE_NUM_REGS,
      CU_FUNC_ATTRIBUTE_PTX_VERSION,
      CU_FUNC_ATTRIBUTE_BINARY_VERSION,
      CU_FUNC_ATTRIBUTE_MAX);
   pragma Convention (C, CUfunction_attribute_enum);  -- cuda.h:311

   subtype CUfunction_attribute is CUfunction_attribute_enum;

   type CUfunc_cache_enum is
     (CU_FUNC_CACHE_PREFER_NONE,
      CU_FUNC_CACHE_PREFER_SHARED,
      CU_FUNC_CACHE_PREFER_L1);
   pragma Convention (C, CUfunc_cache_enum);  -- cuda.h:366

   subtype CUfunc_cache is CUfunc_cache_enum;

   subtype CUmemorytype_enum is unsigned;
   CU_MEMORYTYPE_HOST : constant CUmemorytype_enum := 1;
   CU_MEMORYTYPE_DEVICE : constant CUmemorytype_enum := 2;
   CU_MEMORYTYPE_ARRAY : constant CUmemorytype_enum := 3;
   CU_MEMORYTYPE_UNIFIED : constant CUmemorytype_enum := 4;  -- cuda.h:375

   subtype CUmemorytype is CUmemorytype_enum;

   type CUcomputemode_enum is
     (CU_COMPUTEMODE_DEFAULT,
      CU_COMPUTEMODE_EXCLUSIVE,
      CU_COMPUTEMODE_PROHIBITED,
      CU_COMPUTEMODE_EXCLUSIVE_PROCESS);
   pragma Convention (C, CUcomputemode_enum);  -- cuda.h:385

   subtype CUcomputemode is CUcomputemode_enum;

   type CUjit_option_enum is
     (CU_JIT_MAX_REGISTERS,
      CU_JIT_THREADS_PER_BLOCK,
      CU_JIT_WALL_TIME,
      CU_JIT_INFO_LOG_BUFFER,
      CU_JIT_INFO_LOG_BUFFER_SIZE_BYTES,
      CU_JIT_ERROR_LOG_BUFFER,
      CU_JIT_ERROR_LOG_BUFFER_SIZE_BYTES,
      CU_JIT_OPTIMIZATION_LEVEL,
      CU_JIT_TARGET_FROM_CUCONTEXT,
      CU_JIT_TARGET,
      CU_JIT_FALLBACK_STRATEGY);
   pragma Convention (C, CUjit_option_enum);  -- cuda.h:395

   subtype CUjit_option is CUjit_option_enum;

   type CUjit_target_enum is
     (CU_TARGET_COMPUTE_10,
      CU_TARGET_COMPUTE_11,
      CU_TARGET_COMPUTE_12,
      CU_TARGET_COMPUTE_13,
      CU_TARGET_COMPUTE_20,
      CU_TARGET_COMPUTE_21);
   pragma Convention (C, CUjit_target_enum);  -- cuda.h:487

   subtype CUjit_target is CUjit_target_enum;

   type CUjit_fallback_enum is
     (CU_PREFER_PTX,
      CU_PREFER_BINARY);
   pragma Convention (C, CUjit_fallback_enum);  -- cuda.h:500

   subtype CUjit_fallback is CUjit_fallback_enum;

   subtype CUgraphicsRegisterFlags_enum is unsigned;
   CU_GRAPHICS_REGISTER_FLAGS_NONE : constant CUgraphicsRegisterFlags_enum := 0;
   CU_GRAPHICS_REGISTER_FLAGS_READ_ONLY : constant CUgraphicsRegisterFlags_enum := 1;
   CU_GRAPHICS_REGISTER_FLAGS_WRITE_DISCARD : constant CUgraphicsRegisterFlags_enum := 2;
   CU_GRAPHICS_REGISTER_FLAGS_SURFACE_LDST : constant CUgraphicsRegisterFlags_enum := 4;  -- cuda.h:511

   subtype CUgraphicsRegisterFlags is CUgraphicsRegisterFlags_enum;

   type CUgraphicsMapResourceFlags_enum is
     (CU_GRAPHICS_MAP_RESOURCE_FLAGS_NONE,
      CU_GRAPHICS_MAP_RESOURCE_FLAGS_READ_ONLY,
      CU_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD);
   pragma Convention (C, CUgraphicsMapResourceFlags_enum);  -- cuda.h:521

   subtype CUgraphicsMapResourceFlags is CUgraphicsMapResourceFlags_enum;

   type CUarray_cubemap_face_enum is
     (CU_CUBEMAP_FACE_POSITIVE_X,
      CU_CUBEMAP_FACE_NEGATIVE_X,
      CU_CUBEMAP_FACE_POSITIVE_Y,
      CU_CUBEMAP_FACE_NEGATIVE_Y,
      CU_CUBEMAP_FACE_POSITIVE_Z,
      CU_CUBEMAP_FACE_NEGATIVE_Z);
   pragma Convention (C, CUarray_cubemap_face_enum);  -- cuda.h:530

   subtype CUarray_cubemap_face is CUarray_cubemap_face_enum;

   type CUlimit_enum is
     (CU_LIMIT_STACK_SIZE,
      CU_LIMIT_PRINTF_FIFO_SIZE,
      CU_LIMIT_MALLOC_HEAP_SIZE);
   pragma Convention (C, CUlimit_enum);  -- cuda.h:542

   subtype CUlimit is CUlimit_enum;

   subtype cudaError_enum is unsigned;
   CUDA_SUCCESS : constant cudaError_enum := 0;
   CUDA_ERROR_INVALID_VALUE : constant cudaError_enum := 1;
   CUDA_ERROR_OUT_OF_MEMORY : constant cudaError_enum := 2;
   CUDA_ERROR_NOT_INITIALIZED : constant cudaError_enum := 3;
   CUDA_ERROR_DEINITIALIZED : constant cudaError_enum := 4;
   CUDA_ERROR_PROFILER_DISABLED : constant cudaError_enum := 5;
   CUDA_ERROR_PROFILER_NOT_INITIALIZED : constant cudaError_enum := 6;
   CUDA_ERROR_PROFILER_ALREADY_STARTED : constant cudaError_enum := 7;
   CUDA_ERROR_PROFILER_ALREADY_STOPPED : constant cudaError_enum := 8;
   CUDA_ERROR_NO_DEVICE : constant cudaError_enum := 100;
   CUDA_ERROR_INVALID_DEVICE : constant cudaError_enum := 101;
   CUDA_ERROR_INVALID_IMAGE : constant cudaError_enum := 200;
   CUDA_ERROR_INVALID_CONTEXT : constant cudaError_enum := 201;
   CUDA_ERROR_CONTEXT_ALREADY_CURRENT : constant cudaError_enum := 202;
   CUDA_ERROR_MAP_FAILED : constant cudaError_enum := 205;
   CUDA_ERROR_UNMAP_FAILED : constant cudaError_enum := 206;
   CUDA_ERROR_ARRAY_IS_MAPPED : constant cudaError_enum := 207;
   CUDA_ERROR_ALREADY_MAPPED : constant cudaError_enum := 208;
   CUDA_ERROR_NO_BINARY_FOR_GPU : constant cudaError_enum := 209;
   CUDA_ERROR_ALREADY_ACQUIRED : constant cudaError_enum := 210;
   CUDA_ERROR_NOT_MAPPED : constant cudaError_enum := 211;
   CUDA_ERROR_NOT_MAPPED_AS_ARRAY : constant cudaError_enum := 212;
   CUDA_ERROR_NOT_MAPPED_AS_POINTER : constant cudaError_enum := 213;
   CUDA_ERROR_ECC_UNCORRECTABLE : constant cudaError_enum := 214;
   CUDA_ERROR_UNSUPPORTED_LIMIT : constant cudaError_enum := 215;
   CUDA_ERROR_CONTEXT_ALREADY_IN_USE : constant cudaError_enum := 216;
   CUDA_ERROR_INVALID_SOURCE : constant cudaError_enum := 300;
   CUDA_ERROR_FILE_NOT_FOUND : constant cudaError_enum := 301;
   CUDA_ERROR_SHARED_OBJECT_SYMBOL_NOT_FOUND : constant cudaError_enum := 302;
   CUDA_ERROR_SHARED_OBJECT_INIT_FAILED : constant cudaError_enum := 303;
   CUDA_ERROR_OPERATING_SYSTEM : constant cudaError_enum := 304;
   CUDA_ERROR_INVALID_HANDLE : constant cudaError_enum := 400;
   CUDA_ERROR_NOT_FOUND : constant cudaError_enum := 500;
   CUDA_ERROR_NOT_READY : constant cudaError_enum := 600;
   CUDA_ERROR_LAUNCH_FAILED : constant cudaError_enum := 700;
   CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES : constant cudaError_enum := 701;
   CUDA_ERROR_LAUNCH_TIMEOUT : constant cudaError_enum := 702;
   CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING : constant cudaError_enum := 703;
   CUDA_ERROR_PEER_ACCESS_ALREADY_ENABLED : constant cudaError_enum := 704;
   CUDA_ERROR_PEER_ACCESS_NOT_ENABLED : constant cudaError_enum := 705;
   CUDA_ERROR_PRIMARY_CONTEXT_ACTIVE : constant cudaError_enum := 708;
   CUDA_ERROR_CONTEXT_IS_DESTROYED : constant cudaError_enum := 709;
   CUDA_ERROR_UNKNOWN : constant cudaError_enum := 999;  -- cuda.h:551

   subtype CUresult is cudaError_enum;

   type CUDA_MEMCPY2D_st is record
      srcXInBytes : aliased stddef_h.size_t;  -- cuda.h:870
      srcY : aliased stddef_h.size_t;  -- cuda.h:871
      srcMemoryType : aliased CUmemorytype;  -- cuda.h:873
      srcHost : System.Address;  -- cuda.h:874
      srcDevice : aliased CUdeviceptr;  -- cuda.h:875
      srcArray : CUarray;  -- cuda.h:876
      srcPitch : aliased stddef_h.size_t;  -- cuda.h:877
      dstXInBytes : aliased stddef_h.size_t;  -- cuda.h:879
      dstY : aliased stddef_h.size_t;  -- cuda.h:880
      dstMemoryType : aliased CUmemorytype;  -- cuda.h:882
      dstHost : System.Address;  -- cuda.h:883
      dstDevice : aliased CUdeviceptr;  -- cuda.h:884
      dstArray : CUarray;  -- cuda.h:885
      dstPitch : aliased stddef_h.size_t;  -- cuda.h:886
      WidthInBytes : aliased stddef_h.size_t;  -- cuda.h:888
      Height : aliased stddef_h.size_t;  -- cuda.h:889
   end record;
   pragma Convention (C_Pass_By_Copy, CUDA_MEMCPY2D_st);  -- cuda.h:869

   subtype CUDA_MEMCPY2D is CUDA_MEMCPY2D_st;

   type CUDA_MEMCPY3D_st is record
      srcXInBytes : aliased stddef_h.size_t;  -- cuda.h:896
      srcY : aliased stddef_h.size_t;  -- cuda.h:897
      srcZ : aliased stddef_h.size_t;  -- cuda.h:898
      srcLOD : aliased stddef_h.size_t;  -- cuda.h:899
      srcMemoryType : aliased CUmemorytype;  -- cuda.h:900
      srcHost : System.Address;  -- cuda.h:901
      srcDevice : aliased CUdeviceptr;  -- cuda.h:902
      srcArray : CUarray;  -- cuda.h:903
      reserved0 : System.Address;  -- cuda.h:904
      srcPitch : aliased stddef_h.size_t;  -- cuda.h:905
      srcHeight : aliased stddef_h.size_t;  -- cuda.h:906
      dstXInBytes : aliased stddef_h.size_t;  -- cuda.h:908
      dstY : aliased stddef_h.size_t;  -- cuda.h:909
      dstZ : aliased stddef_h.size_t;  -- cuda.h:910
      dstLOD : aliased stddef_h.size_t;  -- cuda.h:911
      dstMemoryType : aliased CUmemorytype;  -- cuda.h:912
      dstHost : System.Address;  -- cuda.h:913
      dstDevice : aliased CUdeviceptr;  -- cuda.h:914
      dstArray : CUarray;  -- cuda.h:915
      reserved1 : System.Address;  -- cuda.h:916
      dstPitch : aliased stddef_h.size_t;  -- cuda.h:917
      dstHeight : aliased stddef_h.size_t;  -- cuda.h:918
      WidthInBytes : aliased stddef_h.size_t;  -- cuda.h:920
      Height : aliased stddef_h.size_t;  -- cuda.h:921
      Depth : aliased stddef_h.size_t;  -- cuda.h:922
   end record;
   pragma Convention (C_Pass_By_Copy, CUDA_MEMCPY3D_st);  -- cuda.h:895

   subtype CUDA_MEMCPY3D is CUDA_MEMCPY3D_st;

   type CUDA_MEMCPY3D_PEER_st is record
      srcXInBytes : aliased stddef_h.size_t;  -- cuda.h:929
      srcY : aliased stddef_h.size_t;  -- cuda.h:930
      srcZ : aliased stddef_h.size_t;  -- cuda.h:931
      srcLOD : aliased stddef_h.size_t;  -- cuda.h:932
      srcMemoryType : aliased CUmemorytype;  -- cuda.h:933
      srcHost : System.Address;  -- cuda.h:934
      srcDevice : aliased CUdeviceptr;  -- cuda.h:935
      srcArray : CUarray;  -- cuda.h:936
      srcContext : CUcontext;  -- cuda.h:937
      srcPitch : aliased stddef_h.size_t;  -- cuda.h:938
      srcHeight : aliased stddef_h.size_t;  -- cuda.h:939
      dstXInBytes : aliased stddef_h.size_t;  -- cuda.h:941
      dstY : aliased stddef_h.size_t;  -- cuda.h:942
      dstZ : aliased stddef_h.size_t;  -- cuda.h:943
      dstLOD : aliased stddef_h.size_t;  -- cuda.h:944
      dstMemoryType : aliased CUmemorytype;  -- cuda.h:945
      dstHost : System.Address;  -- cuda.h:946
      dstDevice : aliased CUdeviceptr;  -- cuda.h:947
      dstArray : CUarray;  -- cuda.h:948
      dstContext : CUcontext;  -- cuda.h:949
      dstPitch : aliased stddef_h.size_t;  -- cuda.h:950
      dstHeight : aliased stddef_h.size_t;  -- cuda.h:951
      WidthInBytes : aliased stddef_h.size_t;  -- cuda.h:953
      Height : aliased stddef_h.size_t;  -- cuda.h:954
      Depth : aliased stddef_h.size_t;  -- cuda.h:955
   end record;
   pragma Convention (C_Pass_By_Copy, CUDA_MEMCPY3D_PEER_st);  -- cuda.h:928

   subtype CUDA_MEMCPY3D_PEER is CUDA_MEMCPY3D_PEER_st;

   type CUDA_ARRAY_DESCRIPTOR_st is record
      Width : aliased stddef_h.size_t;  -- cuda.h:963
      Height : aliased stddef_h.size_t;  -- cuda.h:964
      Format : aliased CUarray_format;  -- cuda.h:966
      NumChannels : aliased unsigned;  -- cuda.h:967
   end record;
   pragma Convention (C_Pass_By_Copy, CUDA_ARRAY_DESCRIPTOR_st);  -- cuda.h:961

   subtype CUDA_ARRAY_DESCRIPTOR is CUDA_ARRAY_DESCRIPTOR_st;

   type CUDA_ARRAY3D_DESCRIPTOR_st is record
      Width : aliased stddef_h.size_t;  -- cuda.h:975
      Height : aliased stddef_h.size_t;  -- cuda.h:976
      Depth : aliased stddef_h.size_t;  -- cuda.h:977
      Format : aliased CUarray_format;  -- cuda.h:979
      NumChannels : aliased unsigned;  -- cuda.h:980
      Flags : aliased unsigned;  -- cuda.h:981
   end record;
   pragma Convention (C_Pass_By_Copy, CUDA_ARRAY3D_DESCRIPTOR_st);  -- cuda.h:973

   subtype CUDA_ARRAY3D_DESCRIPTOR is CUDA_ARRAY3D_DESCRIPTOR_st;

   function cuInit (arg1 : unsigned) return CUresult;  -- cuda.h:1095
   pragma Import (C, cuInit, "cuInit");

   function cuDriverGetVersion (arg1 : access int) return CUresult;  -- cuda.h:1122
   pragma Import (C, cuDriverGetVersion, "cuDriverGetVersion");

   function cuDeviceGet (arg1 : access CUdevice; arg2 : int) return CUresult;  -- cuda.h:1160
   pragma Import (C, cuDeviceGet, "cuDeviceGet");

   function cuDeviceGetCount (arg1 : access int) return CUresult;  -- cuda.h:1186
   pragma Import (C, cuDeviceGetCount, "cuDeviceGetCount");

   function cuDeviceGetName
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : int;
      arg3 : CUdevice) return CUresult;  -- cuda.h:1215
   pragma Import (C, cuDeviceGetName, "cuDeviceGetName");

   function cuDeviceComputeCapability
     (arg1 : access int;
      arg2 : access int;
      arg3 : CUdevice) return CUresult;  -- cuda.h:1244
   pragma Import (C, cuDeviceComputeCapability, "cuDeviceComputeCapability");

   function cuDeviceTotalMem_v2 (arg1 : access stddef_h.size_t; arg2 : CUdevice) return CUresult;  -- cuda.h:1272
   pragma Import (C, cuDeviceTotalMem_v2, "cuDeviceTotalMem_v2");

   function cuDeviceGetProperties (arg1 : access CUdevprop; arg2 : CUdevice) return CUresult;  -- cuda.h:1332
   pragma Import (C, cuDeviceGetProperties, "cuDeviceGetProperties");

   function cuDeviceGetAttribute
     (arg1 : access int;
      arg2 : CUdevice_attribute;
      arg3 : CUdevice) return CUresult;  -- cuda.h:1444
   pragma Import (C, cuDeviceGetAttribute, "cuDeviceGetAttribute");

   function cuCtxCreate_v2
     (arg1 : System.Address;
      arg2 : unsigned;
      arg3 : CUdevice) return CUresult;  -- cuda.h:1544
   pragma Import (C, cuCtxCreate_v2, "cuCtxCreate_v2");

   function cuCtxDestroy_v2 (arg1 : CUcontext) return CUresult;  -- cuda.h:1583
   pragma Import (C, cuCtxDestroy_v2, "cuCtxDestroy_v2");

   function cuCtxAttach (arg1 : System.Address; arg2 : unsigned) return CUresult;  -- cuda.h:1633
   pragma Import (C, cuCtxAttach, "cuCtxAttach");

   function cuCtxDetach (arg1 : CUcontext) return CUresult;  -- cuda.h:1668
   pragma Import (C, cuCtxDetach, "cuCtxDetach");

   function cuCtxPushCurrent_v2 (arg1 : CUcontext) return CUresult;  -- cuda.h:1704
   pragma Import (C, cuCtxPushCurrent_v2, "cuCtxPushCurrent_v2");

   function cuCtxPopCurrent_v2 (arg1 : System.Address) return CUresult;  -- cuda.h:1737
   pragma Import (C, cuCtxPopCurrent_v2, "cuCtxPopCurrent_v2");

   function cuCtxSetCurrent (arg1 : CUcontext) return CUresult;  -- cuda.h:1763
   pragma Import (C, cuCtxSetCurrent, "cuCtxSetCurrent");

   function cuCtxGetCurrent (arg1 : System.Address) return CUresult;  -- cuda.h:1782
   pragma Import (C, cuCtxGetCurrent, "cuCtxGetCurrent");

   function cuCtxGetDevice (arg1 : access CUdevice) return CUresult;  -- cuda.h:1811
   pragma Import (C, cuCtxGetDevice, "cuCtxGetDevice");

   function cuCtxSynchronize return CUresult;  -- cuda.h:1839
   pragma Import (C, cuCtxSynchronize, "cuCtxSynchronize");

   function cuCtxSetLimit (arg1 : CUlimit; arg2 : stddef_h.size_t) return CUresult;  -- cuda.h:1900
   pragma Import (C, cuCtxSetLimit, "cuCtxSetLimit");

   function cuCtxGetLimit (arg1 : access stddef_h.size_t; arg2 : CUlimit) return CUresult;  -- cuda.h:1933
   pragma Import (C, cuCtxGetLimit, "cuCtxGetLimit");

   function cuCtxGetCacheConfig (arg1 : access CUfunc_cache) return CUresult;  -- cuda.h:1974
   pragma Import (C, cuCtxGetCacheConfig, "cuCtxGetCacheConfig");

   function cuCtxSetCacheConfig (arg1 : CUfunc_cache) return CUresult;  -- cuda.h:2022
   pragma Import (C, cuCtxSetCacheConfig, "cuCtxSetCacheConfig");

   function cuCtxGetApiVersion (arg1 : CUcontext; arg2 : access unsigned) return CUresult;  -- cuda.h:2057
   pragma Import (C, cuCtxGetApiVersion, "cuCtxGetApiVersion");

   function cuModuleLoad (arg1 : System.Address; arg2 : Interfaces.C.Strings.chars_ptr) return CUresult;  -- cuda.h:2106
   pragma Import (C, cuModuleLoad, "cuModuleLoad");

   function cuModuleLoadData (arg1 : System.Address; arg2 : System.Address) return CUresult;  -- cuda.h:2140
   pragma Import (C, cuModuleLoadData, "cuModuleLoadData");

   function cuModuleLoadDataEx
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : unsigned;
      arg4 : access CUjit_option;
      arg5 : System.Address) return CUresult;  -- cuda.h:2219
   pragma Import (C, cuModuleLoadDataEx, "cuModuleLoadDataEx");

   function cuModuleLoadFatBinary (arg1 : System.Address; arg2 : System.Address) return CUresult;  -- cuda.h:2259
   pragma Import (C, cuModuleLoadFatBinary, "cuModuleLoadFatBinary");

   function cuModuleUnload (arg1 : CUmodule) return CUresult;  -- cuda.h:2284
   pragma Import (C, cuModuleUnload, "cuModuleUnload");

   function cuModuleGetFunction
     (arg1 : System.Address;
      arg2 : CUmodule;
      arg3 : Interfaces.C.Strings.chars_ptr) return CUresult;  -- cuda.h:2314
   pragma Import (C, cuModuleGetFunction, "cuModuleGetFunction");

   function cuModuleGetGlobal_v2
     (arg1 : access CUdeviceptr;
      arg2 : access stddef_h.size_t;
      arg3 : CUmodule;
      arg4 : Interfaces.C.Strings.chars_ptr) return CUresult;  -- cuda.h:2348
   pragma Import (C, cuModuleGetGlobal_v2, "cuModuleGetGlobal_v2");

   function cuModuleGetTexRef
     (arg1 : System.Address;
      arg2 : CUmodule;
      arg3 : Interfaces.C.Strings.chars_ptr) return CUresult;  -- cuda.h:2382
   pragma Import (C, cuModuleGetTexRef, "cuModuleGetTexRef");

   function cuModuleGetSurfRef
     (arg1 : System.Address;
      arg2 : CUmodule;
      arg3 : Interfaces.C.Strings.chars_ptr) return CUresult;  -- cuda.h:2413
   pragma Import (C, cuModuleGetSurfRef, "cuModuleGetSurfRef");

   function cuMemGetInfo_v2 (arg1 : access stddef_h.size_t; arg2 : access stddef_h.size_t) return CUresult;  -- cuda.h:2456
   pragma Import (C, cuMemGetInfo_v2, "cuMemGetInfo_v2");

   function cuMemAlloc_v2 (arg1 : access CUdeviceptr; arg2 : stddef_h.size_t) return CUresult;  -- cuda.h:2489
   pragma Import (C, cuMemAlloc_v2, "cuMemAlloc_v2");

   function cuMemAllocPitch_v2
     (arg1 : access CUdeviceptr;
      arg2 : access stddef_h.size_t;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : unsigned) return CUresult;  -- cuda.h:2550
   pragma Import (C, cuMemAllocPitch_v2, "cuMemAllocPitch_v2");

   function cuMemFree_v2 (arg1 : CUdeviceptr) return CUresult;  -- cuda.h:2579
   pragma Import (C, cuMemFree_v2, "cuMemFree_v2");

   function cuMemGetAddressRange_v2
     (arg1 : access CUdeviceptr;
      arg2 : access stddef_h.size_t;
      arg3 : CUdeviceptr) return CUresult;  -- cuda.h:2612
   pragma Import (C, cuMemGetAddressRange_v2, "cuMemGetAddressRange_v2");

   function cuMemAllocHost_v2 (arg1 : System.Address; arg2 : stddef_h.size_t) return CUresult;  -- cuda.h:2658
   pragma Import (C, cuMemAllocHost_v2, "cuMemAllocHost_v2");

   function cuMemFreeHost (arg1 : System.Address) return CUresult;  -- cuda.h:2688
   pragma Import (C, cuMemFreeHost, "cuMemFreeHost");

   function cuMemHostAlloc
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : unsigned) return CUresult;  -- cuda.h:2770
   pragma Import (C, cuMemHostAlloc, "cuMemHostAlloc");

   function cuMemHostGetDevicePointer_v2
     (arg1 : access CUdeviceptr;
      arg2 : System.Address;
      arg3 : unsigned) return CUresult;  -- cuda.h:2808
   pragma Import (C, cuMemHostGetDevicePointer_v2, "cuMemHostGetDevicePointer_v2");

   function cuMemHostGetFlags (arg1 : access unsigned; arg2 : System.Address) return CUresult;  -- cuda.h:2833
   pragma Import (C, cuMemHostGetFlags, "cuMemHostGetFlags");

   function cuMemHostRegister
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : unsigned) return CUresult;  -- cuda.h:2896
   pragma Import (C, cuMemHostRegister, "cuMemHostRegister");

   function cuMemHostUnregister (arg1 : System.Address) return CUresult;  -- cuda.h:2919
   pragma Import (C, cuMemHostUnregister, "cuMemHostUnregister");

   function cuMemcpy
     (arg1 : CUdeviceptr;
      arg2 : CUdeviceptr;
      arg3 : stddef_h.size_t) return CUresult;  -- cuda.h:2955
   pragma Import (C, cuMemcpy, "cuMemcpy");

   function cuMemcpyPeer
     (arg1 : CUdeviceptr;
      arg2 : CUcontext;
      arg3 : CUdeviceptr;
      arg4 : CUcontext;
      arg5 : stddef_h.size_t) return CUresult;  -- cuda.h:2988
   pragma Import (C, cuMemcpyPeer, "cuMemcpyPeer");

   function cuMemcpyHtoD_v2
     (arg1 : CUdeviceptr;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return CUresult;  -- cuda.h:3026
   pragma Import (C, cuMemcpyHtoD_v2, "cuMemcpyHtoD_v2");

   function cuMemcpyDtoH_v2
     (arg1 : System.Address;
      arg2 : CUdeviceptr;
      arg3 : stddef_h.size_t) return CUresult;  -- cuda.h:3059
   pragma Import (C, cuMemcpyDtoH_v2, "cuMemcpyDtoH_v2");

   function cuMemcpyDtoD_v2
     (arg1 : CUdeviceptr;
      arg2 : CUdeviceptr;
      arg3 : stddef_h.size_t) return CUresult;  -- cuda.h:3092
   pragma Import (C, cuMemcpyDtoD_v2, "cuMemcpyDtoD_v2");

   function cuMemcpyDtoA_v2
     (arg1 : CUarray;
      arg2 : stddef_h.size_t;
      arg3 : CUdeviceptr;
      arg4 : stddef_h.size_t) return CUresult;  -- cuda.h:3126
   pragma Import (C, cuMemcpyDtoA_v2, "cuMemcpyDtoA_v2");

   function cuMemcpyAtoD_v2
     (arg1 : CUdeviceptr;
      arg2 : CUarray;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t) return CUresult;  -- cuda.h:3162
   pragma Import (C, cuMemcpyAtoD_v2, "cuMemcpyAtoD_v2");

   function cuMemcpyHtoA_v2
     (arg1 : CUarray;
      arg2 : stddef_h.size_t;
      arg3 : System.Address;
      arg4 : stddef_h.size_t) return CUresult;  -- cuda.h:3196
   pragma Import (C, cuMemcpyHtoA_v2, "cuMemcpyHtoA_v2");

   function cuMemcpyAtoH_v2
     (arg1 : System.Address;
      arg2 : CUarray;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t) return CUresult;  -- cuda.h:3230
   pragma Import (C, cuMemcpyAtoH_v2, "cuMemcpyAtoH_v2");

   function cuMemcpyAtoA_v2
     (arg1 : CUarray;
      arg2 : stddef_h.size_t;
      arg3 : CUarray;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t) return CUresult;  -- cuda.h:3268
   pragma Import (C, cuMemcpyAtoA_v2, "cuMemcpyAtoA_v2");

   function cuMemcpy2D_v2 (arg1 : System.Address) return CUresult;  -- cuda.h:3428
   pragma Import (C, cuMemcpy2D_v2, "cuMemcpy2D_v2");

   function cuMemcpy2DUnaligned_v2 (arg1 : System.Address) return CUresult;  -- cuda.h:3586
   pragma Import (C, cuMemcpy2DUnaligned_v2, "cuMemcpy2DUnaligned_v2");

   function cuMemcpy3D_v2 (arg1 : System.Address) return CUresult;  -- cuda.h:3753
   pragma Import (C, cuMemcpy3D_v2, "cuMemcpy3D_v2");

   function cuMemcpy3DPeer (arg1 : System.Address) return CUresult;  -- cuda.h:3784
   pragma Import (C, cuMemcpy3DPeer, "cuMemcpy3DPeer");

   function cuMemcpyAsync
     (arg1 : CUdeviceptr;
      arg2 : CUdeviceptr;
      arg3 : stddef_h.size_t;
      arg4 : CUstream) return CUresult;  -- cuda.h:3824
   pragma Import (C, cuMemcpyAsync, "cuMemcpyAsync");

   function cuMemcpyPeerAsync
     (arg1 : CUdeviceptr;
      arg2 : CUcontext;
      arg3 : CUdeviceptr;
      arg4 : CUcontext;
      arg5 : stddef_h.size_t;
      arg6 : CUstream) return CUresult;  -- cuda.h:3855
   pragma Import (C, cuMemcpyPeerAsync, "cuMemcpyPeerAsync");

   function cuMemcpyHtoDAsync_v2
     (arg1 : CUdeviceptr;
      arg2 : System.Address;
      arg3 : stddef_h.size_t;
      arg4 : CUstream) return CUresult;  -- cuda.h:3897
   pragma Import (C, cuMemcpyHtoDAsync_v2, "cuMemcpyHtoDAsync_v2");

   function cuMemcpyDtoHAsync_v2
     (arg1 : System.Address;
      arg2 : CUdeviceptr;
      arg3 : stddef_h.size_t;
      arg4 : CUstream) return CUresult;  -- cuda.h:3937
   pragma Import (C, cuMemcpyDtoHAsync_v2, "cuMemcpyDtoHAsync_v2");

   function cuMemcpyDtoDAsync_v2
     (arg1 : CUdeviceptr;
      arg2 : CUdeviceptr;
      arg3 : stddef_h.size_t;
      arg4 : CUstream) return CUresult;  -- cuda.h:3974
   pragma Import (C, cuMemcpyDtoDAsync_v2, "cuMemcpyDtoDAsync_v2");

   function cuMemcpyHtoAAsync_v2
     (arg1 : CUarray;
      arg2 : stddef_h.size_t;
      arg3 : System.Address;
      arg4 : stddef_h.size_t;
      arg5 : CUstream) return CUresult;  -- cuda.h:4016
   pragma Import (C, cuMemcpyHtoAAsync_v2, "cuMemcpyHtoAAsync_v2");

   function cuMemcpyAtoHAsync_v2
     (arg1 : System.Address;
      arg2 : CUarray;
      arg3 : stddef_h.size_t;
      arg4 : stddef_h.size_t;
      arg5 : CUstream) return CUresult;  -- cuda.h:4058
   pragma Import (C, cuMemcpyAtoHAsync_v2, "cuMemcpyAtoHAsync_v2");

   function cuMemcpy2DAsync_v2 (arg1 : System.Address; arg2 : CUstream) return CUresult;  -- cuda.h:4229
   pragma Import (C, cuMemcpy2DAsync_v2, "cuMemcpy2DAsync_v2");

   function cuMemcpy3DAsync_v2 (arg1 : System.Address; arg2 : CUstream) return CUresult;  -- cuda.h:4404
   pragma Import (C, cuMemcpy3DAsync_v2, "cuMemcpy3DAsync_v2");

   function cuMemcpy3DPeerAsync (arg1 : System.Address; arg2 : CUstream) return CUresult;  -- cuda.h:4429
   pragma Import (C, cuMemcpy3DPeerAsync, "cuMemcpy3DPeerAsync");

   function cuMemsetD8_v2
     (arg1 : CUdeviceptr;
      arg2 : unsigned_char;
      arg3 : stddef_h.size_t) return CUresult;  -- cuda.h:4464
   pragma Import (C, cuMemsetD8_v2, "cuMemsetD8_v2");

   function cuMemsetD16_v2
     (arg1 : CUdeviceptr;
      arg2 : unsigned_short;
      arg3 : stddef_h.size_t) return CUresult;  -- cuda.h:4497
   pragma Import (C, cuMemsetD16_v2, "cuMemsetD16_v2");

   function cuMemsetD32_v2
     (arg1 : CUdeviceptr;
      arg2 : unsigned;
      arg3 : stddef_h.size_t) return CUresult;  -- cuda.h:4530
   pragma Import (C, cuMemsetD32_v2, "cuMemsetD32_v2");

   function cuMemsetD2D8_v2
     (arg1 : CUdeviceptr;
      arg2 : stddef_h.size_t;
      arg3 : unsigned_char;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t) return CUresult;  -- cuda.h:4568
   pragma Import (C, cuMemsetD2D8_v2, "cuMemsetD2D8_v2");

   function cuMemsetD2D16_v2
     (arg1 : CUdeviceptr;
      arg2 : stddef_h.size_t;
      arg3 : unsigned_short;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t) return CUresult;  -- cuda.h:4606
   pragma Import (C, cuMemsetD2D16_v2, "cuMemsetD2D16_v2");

   function cuMemsetD2D32_v2
     (arg1 : CUdeviceptr;
      arg2 : stddef_h.size_t;
      arg3 : unsigned;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t) return CUresult;  -- cuda.h:4644
   pragma Import (C, cuMemsetD2D32_v2, "cuMemsetD2D32_v2");

   function cuMemsetD8Async
     (arg1 : CUdeviceptr;
      arg2 : unsigned_char;
      arg3 : stddef_h.size_t;
      arg4 : CUstream) return CUresult;  -- cuda.h:4681
   pragma Import (C, cuMemsetD8Async, "cuMemsetD8Async");

   function cuMemsetD16Async
     (arg1 : CUdeviceptr;
      arg2 : unsigned_short;
      arg3 : stddef_h.size_t;
      arg4 : CUstream) return CUresult;  -- cuda.h:4718
   pragma Import (C, cuMemsetD16Async, "cuMemsetD16Async");

   function cuMemsetD32Async
     (arg1 : CUdeviceptr;
      arg2 : unsigned;
      arg3 : stddef_h.size_t;
      arg4 : CUstream) return CUresult;  -- cuda.h:4754
   pragma Import (C, cuMemsetD32Async, "cuMemsetD32Async");

   function cuMemsetD2D8Async
     (arg1 : CUdeviceptr;
      arg2 : stddef_h.size_t;
      arg3 : unsigned_char;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : CUstream) return CUresult;  -- cuda.h:4796
   pragma Import (C, cuMemsetD2D8Async, "cuMemsetD2D8Async");

   function cuMemsetD2D16Async
     (arg1 : CUdeviceptr;
      arg2 : stddef_h.size_t;
      arg3 : unsigned_short;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : CUstream) return CUresult;  -- cuda.h:4838
   pragma Import (C, cuMemsetD2D16Async, "cuMemsetD2D16Async");

   function cuMemsetD2D32Async
     (arg1 : CUdeviceptr;
      arg2 : stddef_h.size_t;
      arg3 : unsigned;
      arg4 : stddef_h.size_t;
      arg5 : stddef_h.size_t;
      arg6 : CUstream) return CUresult;  -- cuda.h:4880
   pragma Import (C, cuMemsetD2D32Async, "cuMemsetD2D32Async");

   function cuArrayCreate_v2 (arg1 : System.Address; arg2 : System.Address) return CUresult;  -- cuda.h:4983
   pragma Import (C, cuArrayCreate_v2, "cuArrayCreate_v2");

   function cuArrayGetDescriptor_v2 (arg1 : access CUDA_ARRAY_DESCRIPTOR; arg2 : CUarray) return CUresult;  -- cuda.h:5016
   pragma Import (C, cuArrayGetDescriptor_v2, "cuArrayGetDescriptor_v2");

   function cuArrayDestroy (arg1 : CUarray) return CUresult;  -- cuda.h:5047
   pragma Import (C, cuArrayDestroy, "cuArrayDestroy");

   function cuArray3DCreate_v2 (arg1 : System.Address; arg2 : System.Address) return CUresult;  -- cuda.h:5155
   pragma Import (C, cuArray3DCreate_v2, "cuArray3DCreate_v2");

   function cuArray3DGetDescriptor_v2 (arg1 : access CUDA_ARRAY3D_DESCRIPTOR; arg2 : CUarray) return CUresult;  -- cuda.h:5191
   pragma Import (C, cuArray3DGetDescriptor_v2, "cuArray3DGetDescriptor_v2");

   function cuPointerGetAttribute
     (arg1 : System.Address;
      arg2 : CUpointer_attribute;
      arg3 : CUdeviceptr) return CUresult;  -- cuda.h:5398
   pragma Import (C, cuPointerGetAttribute, "cuPointerGetAttribute");

   function cuStreamCreate (arg1 : System.Address; arg2 : unsigned) return CUresult;  -- cuda.h:5435
   pragma Import (C, cuStreamCreate, "cuStreamCreate");

   function cuStreamWaitEvent
     (arg1 : CUstream;
      arg2 : CUevent;
      arg3 : unsigned) return CUresult;  -- cuda.h:5477
   pragma Import (C, cuStreamWaitEvent, "cuStreamWaitEvent");

   function cuStreamQuery (arg1 : CUstream) return CUresult;  -- cuda.h:5501
   pragma Import (C, cuStreamQuery, "cuStreamQuery");

   function cuStreamSynchronize (arg1 : CUstream) return CUresult;  -- cuda.h:5526
   pragma Import (C, cuStreamSynchronize, "cuStreamSynchronize");

   function cuStreamDestroy_v2 (arg1 : CUstream) return CUresult;  -- cuda.h:5554
   pragma Import (C, cuStreamDestroy_v2, "cuStreamDestroy_v2");

   function cuEventCreate (arg1 : System.Address; arg2 : unsigned) return CUresult;  -- cuda.h:5603
   pragma Import (C, cuEventCreate, "cuEventCreate");

   function cuEventRecord (arg1 : CUevent; arg2 : CUstream) return CUresult;  -- cuda.h:5641
   pragma Import (C, cuEventRecord, "cuEventRecord");

   function cuEventQuery (arg1 : CUevent) return CUresult;  -- cuda.h:5672
   pragma Import (C, cuEventQuery, "cuEventQuery");

   function cuEventSynchronize (arg1 : CUevent) return CUresult;  -- cuda.h:5706
   pragma Import (C, cuEventSynchronize, "cuEventSynchronize");

   function cuEventDestroy_v2 (arg1 : CUevent) return CUresult;  -- cuda.h:5735
   pragma Import (C, cuEventDestroy_v2, "cuEventDestroy_v2");

   function cuEventElapsedTime
     (arg1 : access float;
      arg2 : CUevent;
      arg3 : CUevent) return CUresult;  -- cuda.h:5779
   pragma Import (C, cuEventElapsedTime, "cuEventElapsedTime");

   function cuFuncGetAttribute
     (arg1 : access int;
      arg2 : CUfunction_attribute;
      arg3 : CUfunction) return CUresult;  -- cuda.h:5842
   pragma Import (C, cuFuncGetAttribute, "cuFuncGetAttribute");

   function cuFuncSetCacheConfig (arg1 : CUfunction; arg2 : CUfunc_cache) return CUresult;  -- cuda.h:5883
   pragma Import (C, cuFuncSetCacheConfig, "cuFuncSetCacheConfig");

   function cuLaunchKernel
     (arg1 : CUfunction;
      arg2 : unsigned;
      arg3 : unsigned;
      arg4 : unsigned;
      arg5 : unsigned;
      arg6 : unsigned;
      arg7 : unsigned;
      arg8 : unsigned;
      arg9 : CUstream;
      arg10 : System.Address;
      arg11 : System.Address) return CUresult;  -- cuda.h:5999
   pragma Import (C, cuLaunchKernel, "cuLaunchKernel");

   function cuFuncSetBlockShape
     (arg1 : CUfunction;
      arg2 : int;
      arg3 : int;
      arg4 : int) return CUresult;  -- cuda.h:6055
   pragma Import (C, cuFuncSetBlockShape, "cuFuncSetBlockShape");

   function cuFuncSetSharedSize (arg1 : CUfunction; arg2 : unsigned) return CUresult;  -- cuda.h:6089
   pragma Import (C, cuFuncSetSharedSize, "cuFuncSetSharedSize");

   function cuParamSetSize (arg1 : CUfunction; arg2 : unsigned) return CUresult;  -- cuda.h:6121
   pragma Import (C, cuParamSetSize, "cuParamSetSize");

   function cuParamSeti
     (arg1 : CUfunction;
      arg2 : int;
      arg3 : unsigned) return CUresult;  -- cuda.h:6154
   pragma Import (C, cuParamSeti, "cuParamSeti");

   function cuParamSetf
     (arg1 : CUfunction;
      arg2 : int;
      arg3 : float) return CUresult;  -- cuda.h:6187
   pragma Import (C, cuParamSetf, "cuParamSetf");

   function cuParamSetv
     (arg1 : CUfunction;
      arg2 : int;
      arg3 : System.Address;
      arg4 : unsigned) return CUresult;  -- cuda.h:6222
   pragma Import (C, cuParamSetv, "cuParamSetv");

   function cuLaunch (arg1 : CUfunction) return CUresult;  -- cuda.h:6259
   pragma Import (C, cuLaunch, "cuLaunch");

   function cuLaunchGrid
     (arg1 : CUfunction;
      arg2 : int;
      arg3 : int) return CUresult;  -- cuda.h:6298
   pragma Import (C, cuLaunchGrid, "cuLaunchGrid");

   function cuLaunchGridAsync
     (arg1 : CUfunction;
      arg2 : int;
      arg3 : int;
      arg4 : CUstream) return CUresult;  -- cuda.h:6342
   pragma Import (C, cuLaunchGridAsync, "cuLaunchGridAsync");

   function cuParamSetTexRef
     (arg1 : CUfunction;
      arg2 : int;
      arg3 : CUtexref) return CUresult;  -- cuda.h:6367
   pragma Import (C, cuParamSetTexRef, "cuParamSetTexRef");

   function cuTexRefSetArray
     (arg1 : CUtexref;
      arg2 : CUarray;
      arg3 : unsigned) return CUresult;  -- cuda.h:6408
   pragma Import (C, cuTexRefSetArray, "cuTexRefSetArray");

   function cuTexRefSetAddress_v2
     (arg1 : access stddef_h.size_t;
      arg2 : CUtexref;
      arg3 : CUdeviceptr;
      arg4 : stddef_h.size_t) return CUresult;  -- cuda.h:6446
   pragma Import (C, cuTexRefSetAddress_v2, "cuTexRefSetAddress_v2");

   function cuTexRefSetAddress2D_v2
     (arg1 : CUtexref;
      arg2 : System.Address;
      arg3 : CUdeviceptr;
      arg4 : stddef_h.size_t) return CUresult;  -- cuda.h:6487
   pragma Import (C, cuTexRefSetAddress2D_v2, "cuTexRefSetAddress2D_v2");

   function cuTexRefSetFormat
     (arg1 : CUtexref;
      arg2 : CUarray_format;
      arg3 : int) return CUresult;  -- cuda.h:6516
   pragma Import (C, cuTexRefSetFormat, "cuTexRefSetFormat");

   function cuTexRefSetAddressMode
     (arg1 : CUtexref;
      arg2 : int;
      arg3 : CUaddress_mode) return CUresult;  -- cuda.h:6556
   pragma Import (C, cuTexRefSetAddressMode, "cuTexRefSetAddressMode");

   function cuTexRefSetFilterMode (arg1 : CUtexref; arg2 : CUfilter_mode) return CUresult;  -- cuda.h:6589
   pragma Import (C, cuTexRefSetFilterMode, "cuTexRefSetFilterMode");

   function cuTexRefSetFlags (arg1 : CUtexref; arg2 : unsigned) return CUresult;  -- cuda.h:6621
   pragma Import (C, cuTexRefSetFlags, "cuTexRefSetFlags");

   function cuTexRefGetAddress_v2 (arg1 : access CUdeviceptr; arg2 : CUtexref) return CUresult;  -- cuda.h:6647
   pragma Import (C, cuTexRefGetAddress_v2, "cuTexRefGetAddress_v2");

   function cuTexRefGetArray (arg1 : System.Address; arg2 : CUtexref) return CUresult;  -- cuda.h:6673
   pragma Import (C, cuTexRefGetArray, "cuTexRefGetArray");

   function cuTexRefGetAddressMode
     (arg1 : access CUaddress_mode;
      arg2 : CUtexref;
      arg3 : int) return CUresult;  -- cuda.h:6699
   pragma Import (C, cuTexRefGetAddressMode, "cuTexRefGetAddressMode");

   function cuTexRefGetFilterMode (arg1 : access CUfilter_mode; arg2 : CUtexref) return CUresult;  -- cuda.h:6723
   pragma Import (C, cuTexRefGetFilterMode, "cuTexRefGetFilterMode");

   function cuTexRefGetFormat
     (arg1 : access CUarray_format;
      arg2 : access int;
      arg3 : CUtexref) return CUresult;  -- cuda.h:6749
   pragma Import (C, cuTexRefGetFormat, "cuTexRefGetFormat");

   function cuTexRefGetFlags (arg1 : access unsigned; arg2 : CUtexref) return CUresult;  -- cuda.h:6772
   pragma Import (C, cuTexRefGetFlags, "cuTexRefGetFlags");

   function cuTexRefCreate (arg1 : System.Address) return CUresult;  -- cuda.h:6806
   pragma Import (C, cuTexRefCreate, "cuTexRefCreate");

   function cuTexRefDestroy (arg1 : CUtexref) return CUresult;  -- cuda.h:6826
   pragma Import (C, cuTexRefDestroy, "cuTexRefDestroy");

   function cuSurfRefSetArray
     (arg1 : CUsurfref;
      arg2 : CUarray;
      arg3 : unsigned) return CUresult;  -- cuda.h:6864
   pragma Import (C, cuSurfRefSetArray, "cuSurfRefSetArray");

   function cuSurfRefGetArray (arg1 : System.Address; arg2 : CUsurfref) return CUresult;  -- cuda.h:6885
   pragma Import (C, cuSurfRefGetArray, "cuSurfRefGetArray");

   function cuDeviceCanAccessPeer
     (arg1 : access int;
      arg2 : CUdevice;
      arg3 : CUdevice) return CUresult;  -- cuda.h:6923
   pragma Import (C, cuDeviceCanAccessPeer, "cuDeviceCanAccessPeer");

   function cuCtxEnablePeerAccess (arg1 : CUcontext; arg2 : unsigned) return CUresult;  -- cuda.h:6966
   pragma Import (C, cuCtxEnablePeerAccess, "cuCtxEnablePeerAccess");

   function cuCtxDisablePeerAccess (arg1 : CUcontext) return CUresult;  -- cuda.h:6991
   pragma Import (C, cuCtxDisablePeerAccess, "cuCtxDisablePeerAccess");

   function cuGraphicsUnregisterResource (arg1 : CUgraphicsResource) return CUresult;  -- cuda.h:7032
   pragma Import (C, cuGraphicsUnregisterResource, "cuGraphicsUnregisterResource");

   function cuGraphicsSubResourceGetMappedArray
     (arg1 : System.Address;
      arg2 : CUgraphicsResource;
      arg3 : unsigned;
      arg4 : unsigned) return CUresult;  -- cuda.h:7070
   pragma Import (C, cuGraphicsSubResourceGetMappedArray, "cuGraphicsSubResourceGetMappedArray");

   function cuGraphicsResourceGetMappedPointer_v2
     (arg1 : access CUdeviceptr;
      arg2 : access stddef_h.size_t;
      arg3 : CUgraphicsResource) return CUresult;  -- cuda.h:7104
   pragma Import (C, cuGraphicsResourceGetMappedPointer_v2, "cuGraphicsResourceGetMappedPointer_v2");

   function cuGraphicsResourceSetMapFlags (arg1 : CUgraphicsResource; arg2 : unsigned) return CUresult;  -- cuda.h:7145
   pragma Import (C, cuGraphicsResourceSetMapFlags, "cuGraphicsResourceSetMapFlags");

   function cuGraphicsMapResources
     (arg1 : unsigned;
      arg2 : System.Address;
      arg3 : CUstream) return CUresult;  -- cuda.h:7183
   pragma Import (C, cuGraphicsMapResources, "cuGraphicsMapResources");

   function cuGraphicsUnmapResources
     (arg1 : unsigned;
      arg2 : System.Address;
      arg3 : CUstream) return CUresult;  -- cuda.h:7218
   pragma Import (C, cuGraphicsUnmapResources, "cuGraphicsUnmapResources");

   function cuGetExportTable (arg1 : System.Address; arg2 : System.Address) return CUresult;  -- cuda.h:7222
   pragma Import (C, cuGetExportTable, "cuGetExportTable");

end cuda_h;
