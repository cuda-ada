--
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Text_IO;

with CUDA.Driver;

procedure Enum_Devices
is
   use CUDA.Driver;

   procedure Print_Device (Dev : Device_Type);
   --  Print properties of given CUDA device to stdout.

   procedure Print_Device (Dev : Device_Type)
   is
      use Ada.Text_IO;

      MTD : constant Nat_3_Array := Max_Thread_Dimensions (Dev);
      MGS : constant Nat_3_Array := Max_Grid_Size (Dev);
   begin
      New_Line;
      Put_Line ("Name                  : " & Name (Dev));
      Put_Line ("Compute capability    :" & Compute_Capability
                (Dev)'Img (1 .. 4));
      Put_Line ("Clock rate            :" & Clock_Rate (Dev)'Img);
      Put_Line ("Device copy overlap   : " & Copy_Overlap (Dev)'Img);
      Put_Line ("Kernel exec timeout   : " & Kernel_Exec_Timeout (Dev)'Img);
      Put_Line ("Total global mem      :" & Total_Global_Mem (Dev)'Img);
      Put_Line ("Total constant mem    :" & Total_Constant_Mem (Dev)'Img);
      Put_Line ("Max mem pitch         :" & Max_Mem_Pitch (Dev)'Img);
      Put_Line ("Texture alignment     :" & Texture_Alignment (Dev)'Img);
      Put_Line ("Multiprocessor count  :" & Multiprocessor_Count (Dev)'Img);
      Put_Line ("Shared mem per mp     :" & Shared_Mem_Per_Block (Dev)'Img);
      Put_Line ("Registers per mp      :" & Regs_Per_Block (Dev)'Img);
      Put_Line ("Threads in warp       :" & Warp_Size (Dev)'Img);
      Put_Line ("Max threads per block :" & Max_Threads_Per_Block (Dev)'Img);
      Put_Line ("Max thread dimensions : ("
                & MTD (1)'Img & "," & MTD (2)'Img & "," & MTD (3)'Img & " )");
      Put_Line ("Max grid size         : ("
                & MGS (1)'Img & "," & MGS (2)'Img & "," & MGS (3)'Img & " )");
   end Print_Device;
begin
   Ada.Text_IO.Put_Line ("Found" & Device_Count'Img & " CUDA device(s):");
   Iterate (Process => Print_Device'Access);
end Enum_Devices;
