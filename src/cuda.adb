--
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with driver_types_h;

package body CUDA is

   function Error_To_String (Code : cuda_h.CUresult) return String;
   --  Return error string for given CUDA error code.

   -------------------------------------------------------------------------

   procedure Check_Result
     (Code : cuda_h.CUresult;
      Msg  : String)
   is
      use type cuda_h.CUresult;
   begin
      if Code /= driver_types_h.cudaSuccess then
         raise CUDA_Error with Msg & " (" & Error_To_String (Code) & ")";
      end if;
   end Check_Result;

   -------------------------------------------------------------------------

   function Error_To_String (Code : cuda_h.CUresult) return String
   is
      use cuda_h;
   begin
      case Code is
         when CUDA_SUCCESS                              =>
            return "Success";
         when CUDA_ERROR_INVALID_VALUE                  =>
            return "Invalid value";
         when CUDA_ERROR_OUT_OF_MEMORY                  =>
            return "Out of memory";
         when CUDA_ERROR_NOT_INITIALIZED                =>
            return "Not initialized";
         when CUDA_ERROR_DEINITIALIZED                  =>
            return "Deinitialized";
         when CUDA_ERROR_PROFILER_DISABLED              =>
            return "Profiler disabled";
         when CUDA_ERROR_PROFILER_NOT_INITIALIZED       =>
            return "Profiler not initialized";
         when CUDA_ERROR_PROFILER_ALREADY_STARTED       =>
            return "Profiler already started";
         when CUDA_ERROR_PROFILER_ALREADY_STOPPED       =>
            return "Profiler already stopped";
         when CUDA_ERROR_NO_DEVICE                      =>
            return "No device";
         when CUDA_ERROR_INVALID_DEVICE                 =>
            return "Invalid device";
         when CUDA_ERROR_INVALID_IMAGE                  =>
            return "Invalid image";
         when CUDA_ERROR_INVALID_CONTEXT                =>
            return "Invalid context";
         when CUDA_ERROR_CONTEXT_ALREADY_CURRENT        =>
            return "Context already current";
         when CUDA_ERROR_MAP_FAILED                     =>
            return "Map failed";
         when CUDA_ERROR_UNMAP_FAILED                   =>
            return "Unmap failed";
         when CUDA_ERROR_ARRAY_IS_MAPPED                =>
            return "Array is mapped";
         when CUDA_ERROR_ALREADY_MAPPED                 =>
            return "Already mapped";
         when CUDA_ERROR_NO_BINARY_FOR_GPU              =>
            return "No binary for GPU";
         when CUDA_ERROR_ALREADY_ACQUIRED               =>
            return "Already acquired";
         when CUDA_ERROR_NOT_MAPPED                     =>
            return "Not mapped";
         when CUDA_ERROR_NOT_MAPPED_AS_ARRAY            =>
            return "Not mapped as array";
         when CUDA_ERROR_NOT_MAPPED_AS_POINTER          =>
            return "Not mapped as pointer";
         when CUDA_ERROR_ECC_UNCORRECTABLE              =>
            return "ECC uncorrectable";
         when CUDA_ERROR_UNSUPPORTED_LIMIT              =>
            return "Unsupported limit";
         when CUDA_ERROR_CONTEXT_ALREADY_IN_USE         =>
            return "Context already in use";
         when CUDA_ERROR_INVALID_SOURCE                 =>
            return "Invalid source";
         when CUDA_ERROR_FILE_NOT_FOUND                 =>
            return "File not found";
         when CUDA_ERROR_SHARED_OBJECT_SYMBOL_NOT_FOUND =>
            return "Shared object symbol not found";
         when CUDA_ERROR_SHARED_OBJECT_INIT_FAILED      =>
            return "Shared object init failed";
         when CUDA_ERROR_OPERATING_SYSTEM               =>
            return "OS call failed";
         when CUDA_ERROR_INVALID_HANDLE                 =>
            return "Invalid handle";
         when CUDA_ERROR_NOT_FOUND                      =>
            return "Not found";
         when CUDA_ERROR_NOT_READY                      =>
            return "Not ready";
         when CUDA_ERROR_LAUNCH_FAILED                  =>
            return "Launch failed";
         when CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES        =>
            return "Launch out of resources";
         when CUDA_ERROR_LAUNCH_TIMEOUT                 =>
            return "Launch timeout";
         when CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING  =>
            return "Launch incompatible texturing";
         when CUDA_ERROR_PEER_ACCESS_ALREADY_ENABLED    =>
            return "Peer access already enabled";
         when CUDA_ERROR_PEER_ACCESS_NOT_ENABLED        =>
            return "Peer access not enabled";
         when CUDA_ERROR_PRIMARY_CONTEXT_ACTIVE         =>
            return "Primary context active";
         when CUDA_ERROR_CONTEXT_IS_DESTROYED           =>
            return "Context is destroyed";
         when CUDA_ERROR_UNKNOWN                        =>
            return "Unknown";
         when others                                    =>
            return "Invalid error code";
      end case;
   end Error_To_String;

end CUDA;
