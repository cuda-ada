with Interfaces.C; use Interfaces.C;
with driver_types_h;

package texture_types_h is


   cudaTextureType1D : constant := 16#01#;  --  texture_types.h:73
   cudaTextureType2D : constant := 16#02#;  --  texture_types.h:74
   cudaTextureType3D : constant := 16#03#;  --  texture_types.h:75
   cudaTextureType1DLayered : constant := 16#F1#;  --  texture_types.h:76
   cudaTextureType2DLayered : constant := 16#F2#;  --  texture_types.h:77

   type cudaTextureAddressMode is 
     (cudaAddressModeWrap,
      cudaAddressModeClamp,
      cudaAddressModeMirror,
      cudaAddressModeBorder);
   pragma Convention (C, cudaTextureAddressMode);  -- texture_types.h:83

   type cudaTextureFilterMode is 
     (cudaFilterModePoint,
      cudaFilterModeLinear);
   pragma Convention (C, cudaTextureFilterMode);  -- texture_types.h:95

   type cudaTextureReadMode is 
     (cudaReadModeElementType,
      cudaReadModeNormalizedFloat);
   pragma Convention (C, cudaTextureReadMode);  -- texture_types.h:105

   type anon1000_anon1002_array is array (0 .. 2) of aliased cudaTextureAddressMode;
   type anon1000_anon1005_array is array (0 .. 14) of aliased int;
   type textureReference is record
      normalized : aliased int;  -- texture_types.h:120
      filterMode : aliased cudaTextureFilterMode;  -- texture_types.h:124
      addressMode : aliased anon1000_anon1002_array;  -- texture_types.h:128
      channelDesc : aliased driver_types_h.cudaChannelFormatDesc;  -- texture_types.h:132
      sRGB : aliased int;  -- texture_types.h:136
      uu_cudaReserved : aliased anon1000_anon1005_array;  -- texture_types.h:137
   end record;
   pragma Convention (C_Pass_By_Copy, textureReference);  -- texture_types.h:115

end texture_types_h;
