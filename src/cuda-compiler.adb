--
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Directories;
with Ada.Direct_IO;

with GNAT.SHA1;
with GNAT.OS_Lib;

with Interfaces.C.Strings;

with Templates_Parser;

with CUDA.Logger;

package body CUDA.Compiler is

   package IC renames Interfaces.C;

   package L renames CUDA.Logger;
   package D_IO is new Ada.Direct_IO (Element_Type => IC.char);

   Nvcc : constant String :=
     "/usr/local/cuda/bin/nvcc";

   Cache_Dir : constant String := "./cache";

   type Arg_Mode_Type is (In_Mode, In_Out_Mode, Out_Mode);
   --  Argument mode type.

   type Kernel_Param_Type is array (Positive range <>) of System.Address;
   --  CUDA kernel parameters.

   type Buffer_Type is array (Positive range <>) of aliased Interfaces.C.char;
   --  Buffer type used to store compiled cubin.

   function Read (Filename : String) return Buffer_Type;
   --  Read data from given file and return it in buffer.

   function Compile
     (Source   : String;
      Filename : String)
      return Buffer_Type;
   --  Compile given CUDA code to cubin and return it in buffer. Store the
   --  compiled code as file with given filename in the cache.

   procedure Init
     (Arg    : in out Arg_Type;
      Size   :        stddef_h.size_t;
      Data   :        System.Address := System.Null_Address;
      Mode   :        Arg_Mode_Type);
   --  Initialize argument object with given size and data. Allocate device
   --  memory and copy data to device for modes 'in' and 'in out'.

   package body Arg_Creators is

      ----------------------------------------------------------------------

      function In_Arg (Data : Data_Type) return Arg_Type
      is
      begin
         return Arg : Arg_Type do
            Init (Arg  => Arg,
                  Size => Data'Size,
                  Data => Data'Address,
                  Mode => In_Mode);
         end return;
      end In_Arg;

      ----------------------------------------------------------------------

      function In_Out_Arg (Data : not null access Data_Type) return Arg_Type
      is
      begin
         return Arg : Arg_Type do
            Init (Arg  => Arg,
                  Size => Data.all'Size,
                  Data => Data.all'Address,
                  Mode => In_Out_Mode);
         end return;
      end In_Out_Arg;

      ----------------------------------------------------------------------

      function Out_Arg (Data : not null access Data_Type) return Arg_Type
      is
      begin
         return Arg : Arg_Type do
            Init (Arg    => Arg,
                  Size   => Data.all'Size,
                  Data   => Data.all'Address,
                  Mode   => Out_Mode);
         end return;
      end Out_Arg;

   end Arg_Creators;

   -------------------------------------------------------------------------

   procedure Call
     (Func        : Function_Type;
      Args        : Kernel_Args_Type;
      Grid_Dim_X  : Positive := 128;
      Grid_Dim_Y  : Positive := 1;
      Grid_Dim_Z  : Positive := 1;
      Block_Dim_X : Positive := 1;
      Block_Dim_Y : Positive := 1;
      Block_Dim_Z : Positive := 1)
   is
      Kernel_Args : Kernel_Param_Type (1 .. Args'Length);
   begin
      for I in Args'Range loop
         Kernel_Args (I) := Args (I).Dev_Data'Address;
      end loop;

      Check_Result (Code => cuda_h.cuLaunchKernel
                    (arg1  => Func.CU_Function,
                     arg2  => IC.unsigned (Grid_Dim_X),
                     arg3  => IC.unsigned (Grid_Dim_Y),
                     arg4  => IC.unsigned (Grid_Dim_Z),
                     arg5  => IC.unsigned (Block_Dim_X),
                     arg6  => IC.unsigned (Block_Dim_Y),
                     arg7  => IC.unsigned (Block_Dim_Z),
                     arg8  => 0,
                     arg9  => cuda_h.CUstream (System.Null_Address),
                     arg10 => Kernel_Args'Address,
                     arg11 => System.Null_Address),
                    Msg  => "Could not launch function");
   end Call;

   -------------------------------------------------------------------------

   function Compile (Source : Source_Module_Type) return Module_Type
   is
      use GNAT;

      S : constant String              := Get_Code (Source);
      D : constant SHA1.Message_Digest := GNAT.SHA1.Digest (S);
      B : constant Buffer_Type         := Compile (Source   => S,
                                                   Filename => D);
   begin
      return M : Module_Type do
         Check_Result (Code => cuda_h.cuModuleLoadData
                       (arg1 => M.CU_Module'Address,
                        arg2 => B'Address),
                       Msg  => "Could not load module");
      end return;
   end Compile;

   -------------------------------------------------------------------------

   function Compile
     (Source   : String;
      Filename : String)
      return Buffer_Type
   is
      Source_Path : constant String := Cache_Dir & "/" & Filename & ".cu";
      Kernel_Path : constant String := Cache_Dir & "/" & Filename & ".cubin";
      File        : D_IO.File_Type;
   begin
      if not Ada.Directories.Exists (Name => Cache_Dir) then
         pragma Debug (L.Log ("Creating cache directory " & Cache_Dir));
         Ada.Directories.Create_Directory (New_Directory => Cache_Dir);
      elsif Ada.Directories.Exists (Name => Kernel_Path) then
         pragma Debug (L.Log ("Cache hit for source module " & Filename));
         return Read (Filename => Kernel_Path);
      end if;

      D_IO.Create (File => File,
                   Mode => D_IO.Out_File,
                   Name => Source_Path);

      for I in Source'Range loop
         D_IO.Write (File => File,
                     Item => IC.char (Source (I)));
      end loop;

      D_IO.Close (File => File);
      pragma Debug (L.Log ("Source module stored in " & Source_Path));

      declare
         Args : GNAT.OS_Lib.Argument_List (1 .. 4);
         Res  : Boolean;
      begin
         Args (1) := new String'("-cubin");
         Args (2) := new String'(Source_Path);
         Args (3) := new String'("-o");
         Args (4) := new String'(Kernel_Path);
         GNAT.OS_Lib.Spawn (Program_Name => Nvcc,
                            Args         => Args,
                            Success      => Res);

         if not Res then
            raise Program_Error with "Error calling nvcc";
         end if;
      end;
      pragma Debug (L.Log ("Compiled module stored in " & Kernel_Path));

      return Read (Filename => Kernel_Path);
   end Compile;

   -------------------------------------------------------------------------

   function Create
     (Preamble  : String := "";
      Operation : String)
      return Source_Module_Type
   is
      Trans  : constant Templates_Parser.Translate_Table
        := (1 => Templates_Parser.Assoc ("PREAMBLE", Preamble),
            2 => Templates_Parser.Assoc ("OP", Operation));
      Source : constant String := "@_PREAMBLE_@" & ASCII.LF
        & "extern ""C"" @_OP_@";
      Module : Source_Module_Type;
   begin
      Module.Code := Ada.Strings.Unbounded.To_Unbounded_String
        (Templates_Parser.Translate
           (Template     => Source,
            Translations => Trans));

      return Module;
   end Create;

   -------------------------------------------------------------------------

   procedure Finalize (Object : in out Arg_Type)
   is
      use type System.Address;
   begin
      if Object.Host_Data /= System.Null_Address then
         begin
            Check_Result
              (Code => cuda_h.cuMemcpyDtoH_v2
                 (arg1 => Object.Host_Data,
                  arg2 => Object.Dev_Data,
                  arg3 => Object.Data_Size),
               Msg  => "Unable to copy output variable from device");

         exception
            when others =>
               Check_Result
                 (Code => cuda_h.cuMemFree_v2 (arg1 => Object.Dev_Data),
                  Msg  => "Error freeing device memory");
               raise;
         end;
      end if;

      Check_Result
        (Code => cuda_h.cuMemFree_v2 (arg1 => Object.Dev_Data),
         Msg  => "Error freeing device memory");
   end Finalize;

   -------------------------------------------------------------------------

   function Get_Code (Src_Module : Source_Module_Type) return String
   is
   begin
      return Ada.Strings.Unbounded.To_String (Src_Module.Code);
   end Get_Code;

   -------------------------------------------------------------------------

   function Get_Function
     (Module : Module_Type;
      Name   : String)
      return Function_Type
   is
   begin
      return F : Function_Type do
         Check_Result (Code => cuda_h.cuModuleGetFunction
                       (arg1 => F.CU_Function'Address,
                        arg2 => Module.CU_Module,
                        arg3 => IC.Strings.New_String (Name)),
                       Msg  => "Could not get function " & Name);
         pragma Debug (L.Log ("Returning function '" & Name & "'"));
      end return;
   end Get_Function;

   -------------------------------------------------------------------------

   procedure Init
     (Arg    : in out Arg_Type;
      Size   :        stddef_h.size_t;
      Data   :        System.Address := System.Null_Address;
      Mode   :        Arg_Mode_Type)
   is
      use type stddef_h.size_t;
   begin
      Arg.Data_Size := Size / 8;
      Check_Result
        (Code => cuda_h.cuMemAlloc_v2
           (arg1 => Arg.Dev_Data'Access,
            arg2 => Arg.Data_Size),
         Msg  => "Unable to allocate" & Arg.Data_Size'Img
         & " bytes of device memory");

      if Mode = In_Mode or Mode = In_Out_Mode then
         Check_Result
           (Code => cuda_h.cuMemcpyHtoD_v2
              (arg1 => Arg.Dev_Data,
               arg2 => Data,
               arg3 => Arg.Data_Size),
            Msg  => "Unable to copy variable to device");
      end if;

      if Mode = In_Out_Mode or Mode = Out_Mode then
         Arg.Host_Data := Data;
      end if;
   end Init;

   -------------------------------------------------------------------------

   function Read (Filename : String) return Buffer_Type
   is
      File : D_IO.File_Type;
   begin
      D_IO.Open (File => File,
                 Mode => D_IO.In_File,
                 Name => Filename);

      declare
         Result : Buffer_Type (1 .. Positive (D_IO.Size (File => File)));
      begin
         for I in Result'Range loop
            D_IO.Read (File => File,
                       Item => Result (I));
         end loop;

         D_IO.Close (File => File);

         return Result;
      end;
   end Read;

end CUDA.Compiler;
