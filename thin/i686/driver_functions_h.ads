with Interfaces.C; use Interfaces.C;
with System;
with stddef_h;
with driver_types_h;

package driver_functions_h is

   function make_cudaPitchedPtr
     (d : System.Address;
      p : stddef_h.size_t;
      xsz : stddef_h.size_t;
      ysz : stddef_h.size_t) return driver_types_h.cudaPitchedPtr;  -- driver_functions.h:79
   pragma Import (C, make_cudaPitchedPtr, "make_cudaPitchedPtr");

   function make_cudaPos
     (x : stddef_h.size_t;
      y : stddef_h.size_t;
      z : stddef_h.size_t) return driver_types_h.cudaPos;  -- driver_functions.h:106
   pragma Import (C, make_cudaPos, "make_cudaPos");

   function make_cudaExtent
     (w : stddef_h.size_t;
      h : stddef_h.size_t;
      d : stddef_h.size_t) return driver_types_h.cudaExtent;  -- driver_functions.h:132
   pragma Import (C, make_cudaExtent, "make_cudaExtent");

end driver_functions_h;
