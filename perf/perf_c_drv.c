/*
 *  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
 *  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
 *  University of Applied Sciences Rapperswil
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  This software contains source code provided by NVIDIA Corporation.
 *  The CUDA kernel for the matrix multiplication is based on the source
 *  provided in NVIDIA SDK code sample, see:
 *    http://developer.nvidia.com/cuda-cc-sdk-code-samples#matrixMul
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <cuda.h>

#define N          512
#define BLOCK_SIZE 16

/*
 * A file containing a compiled CUBIN kernel containing the 'mul' function is
 * expected at this location. Thus you must run perf_cudaada prior to executing
 * this binary.
 */
static const char* kernel = "cache/719c4fb17ef65aa9b58ab89569e9a096602aa750.cubin";

static CUcontext   context;
static CUfunction  matrixMul = NULL;
static int         device    = 0;
static int         gridsize  = 0;

void check_result(CUresult code, const char* message)
{
	if (code != CUDA_SUCCESS)
	{
		fprintf(stderr, "%s (%d)\n", message, (int) code);
		exit(-1);
	}
}

void cuda_init()
{
	CUmodule module;

	check_result(cuInit(0), "CUDA init failed");
	check_result(cuDeviceGet(&device, 0), "Could not get device");
	check_result(cuCtxCreate(&context, 0, device),
			"Context creation failed");
	check_result(cuModuleLoad(&module, kernel), "Unable to load kernel");

	check_result(cuModuleGetFunction(&matrixMul, module, "mul"),
			"Could not get function mul");
}

void cuda_finalize()
{
	check_result(cuCtxDestroy(context), "Context destroy failed");
}

void gpu_mul(float* A, float* B, float* C)
{
	float *A_Dev, *B_Dev, *C_Dev;
	const size_t mem_size = sizeof(float) * N * N;

	check_result(cuMemAlloc((CUdeviceptr *) &A_Dev, mem_size),
			"Unable to allocate device memory");
	check_result(cuMemAlloc((CUdeviceptr *) &B_Dev, mem_size),
			"Unable to allocate device memory");
	check_result(cuMemAlloc((CUdeviceptr *) &C_Dev, mem_size),
			"Unable to allocate device memory");
	check_result(cuMemcpyHtoD((CUdeviceptr) A_Dev, A, mem_size),
			"Unable to copy variable to device");
	check_result(cuMemcpyHtoD((CUdeviceptr) B_Dev, B, mem_size),
			"Unable to copy variable to device");

	void *args[3] = {&A_Dev, &B_Dev, &C_Dev};
	cuLaunchKernel(matrixMul,
			gridsize, gridsize, 1,
			BLOCK_SIZE, BLOCK_SIZE, 1,
			0, NULL, args, NULL);

	check_result(cuMemcpyDtoH(C, (CUdeviceptr) C_Dev, mem_size),
			"Unable to copy output variable from device");

	check_result(cuMemFree((CUdeviceptr) A_Dev),
			"Error freeing device memory");
	check_result(cuMemFree((CUdeviceptr) B_Dev),
			"Error freeing device memory");
	check_result(cuMemFree((CUdeviceptr) C_Dev),
			"Error freeing device memory");
}

int main(int argc, char** argv)
{
	const unsigned int mem_size = sizeof(float) * N * N;
	int i, count = 1;
	float *A, *B, *C;
	double elapsed_time = 0;
	struct timeval t1, t2;

	cuda_init();

	gridsize = ceil((float)(N) / (float)(BLOCK_SIZE));

	printf("CUDA Driver API reference performance measurement\n");

	if (argc == 2) {
		if ((count = atoi(argv[1])) < 1) {
			fprintf(stderr, "Invalid iteration count\n");
			return -1;
		}
	}

	A = (float*)malloc(mem_size);
	B = (float*)malloc(mem_size);
	C = (float*)malloc(mem_size);

	for (i = 0; i < N * N; ++i) {
		A[i] = B[i] = 2.0;
		C[i] = 0.0;
	}

	printf("Executing %d iteration(s)\n", count);
	printf("- Matrix [1 .. %u, 1 .. %u]\n", N, N);
	printf("- Grid size %u\n", gridsize);
	printf("- Block size %u\n", BLOCK_SIZE);

	for (i = 0; i < count; i++) {
		gettimeofday(&t1, NULL);
		gpu_mul(A, B, C);
		gettimeofday(&t2, NULL);
		elapsed_time = elapsed_time +
			((t2.tv_sec - t1.tv_sec) * 1000.0) +
			((t2.tv_usec - t1.tv_usec) / 1000.0);
	}

	printf("Total CUDA C Driver API execution time : %.9f\n",
			elapsed_time / 1000.0);

	free(A);
	free(B);
	free(C);

	cuda_finalize();

	return 0;
}
