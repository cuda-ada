--
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with Ada.Finalization;

private with Interfaces.C;

private with cuda_h;

package CUDA.Autoinit is

private

   type Autoinit_Type is new Ada.Finalization.Controlled with record
      Context : cuda_h.CUcontext;
      Device  : aliased Interfaces.C.int;
   end record;

   overriding
   procedure Initialize (T : in out Autoinit_Type);
   --  Init CUDA.

   overriding
   procedure Finalize (T : in out Autoinit_Type);
   --  Cleanup CUDA ressources.

end CUDA.Autoinit;
