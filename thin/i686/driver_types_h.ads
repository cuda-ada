with Interfaces.C; use Interfaces.C;
with System;
with stddef_h;

package driver_types_h is


   cudaHostAllocDefault : constant := 16#00#;  --  driver_types.h:77
   cudaHostAllocPortable : constant := 16#01#;  --  driver_types.h:78
   cudaHostAllocMapped : constant := 16#02#;  --  driver_types.h:79
   cudaHostAllocWriteCombined : constant := 16#04#;  --  driver_types.h:80

   cudaHostRegisterDefault : constant := 16#00#;  --  driver_types.h:82
   cudaHostRegisterPortable : constant := 16#01#;  --  driver_types.h:83
   cudaHostRegisterMapped : constant := 16#02#;  --  driver_types.h:84

   cudaPeerAccessDefault : constant := 16#00#;  --  driver_types.h:86

   cudaEventDefault : constant := 16#00#;  --  driver_types.h:88
   cudaEventBlockingSync : constant := 16#01#;  --  driver_types.h:89
   cudaEventDisableTiming : constant := 16#02#;  --  driver_types.h:90

   cudaDeviceScheduleAuto : constant := 16#00#;  --  driver_types.h:92
   cudaDeviceScheduleSpin : constant := 16#01#;  --  driver_types.h:93
   cudaDeviceScheduleYield : constant := 16#02#;  --  driver_types.h:94
   cudaDeviceScheduleBlockingSync : constant := 16#04#;  --  driver_types.h:95
   cudaDeviceBlockingSync : constant := 16#04#;  --  driver_types.h:96
   cudaDeviceMapHost : constant := 16#08#;  --  driver_types.h:97
   cudaDeviceLmemResizeToMax : constant := 16#10#;  --  driver_types.h:98
   cudaDeviceMask : constant := 16#1f#;  --  driver_types.h:99

   cudaArrayDefault : constant := 16#00#;  --  driver_types.h:101
   cudaArrayLayered : constant := 16#01#;  --  driver_types.h:102
   cudaArraySurfaceLoadStore : constant := 16#02#;  --  driver_types.h:103
   --  unsupported macro: cudaDevicePropDontCare { {'\0'}, 0, 0, 0, 0, 0, 0, {0, 0, 0}, {0, 0, 0}, 0, 0, -1, -1, 0, -1, 0, 0, 0, 0, 0, 0, {0, 0}, {0, 0, 0}, {0, 0}, {0, 0, 0}, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }

   subtype cudaError is unsigned;
   cudaSuccess : constant cudaError := 0;
   cudaErrorMissingConfiguration : constant cudaError := 1;
   cudaErrorMemoryAllocation : constant cudaError := 2;
   cudaErrorInitializationError : constant cudaError := 3;
   cudaErrorLaunchFailure : constant cudaError := 4;
   cudaErrorPriorLaunchFailure : constant cudaError := 5;
   cudaErrorLaunchTimeout : constant cudaError := 6;
   cudaErrorLaunchOutOfResources : constant cudaError := 7;
   cudaErrorInvalidDeviceFunction : constant cudaError := 8;
   cudaErrorInvalidConfiguration : constant cudaError := 9;
   cudaErrorInvalidDevice : constant cudaError := 10;
   cudaErrorInvalidValue : constant cudaError := 11;
   cudaErrorInvalidPitchValue : constant cudaError := 12;
   cudaErrorInvalidSymbol : constant cudaError := 13;
   cudaErrorMapBufferObjectFailed : constant cudaError := 14;
   cudaErrorUnmapBufferObjectFailed : constant cudaError := 15;
   cudaErrorInvalidHostPointer : constant cudaError := 16;
   cudaErrorInvalidDevicePointer : constant cudaError := 17;
   cudaErrorInvalidTexture : constant cudaError := 18;
   cudaErrorInvalidTextureBinding : constant cudaError := 19;
   cudaErrorInvalidChannelDescriptor : constant cudaError := 20;
   cudaErrorInvalidMemcpyDirection : constant cudaError := 21;
   cudaErrorAddressOfConstant : constant cudaError := 22;
   cudaErrorTextureFetchFailed : constant cudaError := 23;
   cudaErrorTextureNotBound : constant cudaError := 24;
   cudaErrorSynchronizationError : constant cudaError := 25;
   cudaErrorInvalidFilterSetting : constant cudaError := 26;
   cudaErrorInvalidNormSetting : constant cudaError := 27;
   cudaErrorMixedDeviceExecution : constant cudaError := 28;
   cudaErrorCudartUnloading : constant cudaError := 29;
   cudaErrorUnknown : constant cudaError := 30;
   cudaErrorNotYetImplemented : constant cudaError := 31;
   cudaErrorMemoryValueTooLarge : constant cudaError := 32;
   cudaErrorInvalidResourceHandle : constant cudaError := 33;
   cudaErrorNotReady : constant cudaError := 34;
   cudaErrorInsufficientDriver : constant cudaError := 35;
   cudaErrorSetOnActiveProcess : constant cudaError := 36;
   cudaErrorInvalidSurface : constant cudaError := 37;
   cudaErrorNoDevice : constant cudaError := 38;
   cudaErrorECCUncorrectable : constant cudaError := 39;
   cudaErrorSharedObjectSymbolNotFound : constant cudaError := 40;
   cudaErrorSharedObjectInitFailed : constant cudaError := 41;
   cudaErrorUnsupportedLimit : constant cudaError := 42;
   cudaErrorDuplicateVariableName : constant cudaError := 43;
   cudaErrorDuplicateTextureName : constant cudaError := 44;
   cudaErrorDuplicateSurfaceName : constant cudaError := 45;
   cudaErrorDevicesUnavailable : constant cudaError := 46;
   cudaErrorInvalidKernelImage : constant cudaError := 47;
   cudaErrorNoKernelImageForDevice : constant cudaError := 48;
   cudaErrorIncompatibleDriverContext : constant cudaError := 49;
   cudaErrorPeerAccessAlreadyEnabled : constant cudaError := 50;
   cudaErrorPeerAccessNotEnabled : constant cudaError := 51;
   cudaErrorDeviceAlreadyInUse : constant cudaError := 54;
   cudaErrorProfilerDisabled : constant cudaError := 55;
   cudaErrorProfilerNotInitialized : constant cudaError := 56;
   cudaErrorProfilerAlreadyStarted : constant cudaError := 57;
   cudaErrorProfilerAlreadyStopped : constant cudaError := 58;
   cudaErrorStartupFailure : constant cudaError := 127;
   cudaErrorApiFailureBase : constant cudaError := 10000;  -- driver_types.h:118

   type cudaChannelFormatKind is
     (cudaChannelFormatKindSigned,
      cudaChannelFormatKindUnsigned,
      cudaChannelFormatKindFloat,
      cudaChannelFormatKindNone);
   pragma Convention (C, cudaChannelFormatKind);  -- driver_types.h:541

   type cudaChannelFormatDesc is record
      x : aliased int;  -- driver_types.h:555
      y : aliased int;  -- driver_types.h:556
      z : aliased int;  -- driver_types.h:557
      w : aliased int;  -- driver_types.h:558
      f : aliased cudaChannelFormatKind;  -- driver_types.h:559
   end record;
   pragma Convention (C_Pass_By_Copy, cudaChannelFormatDesc);  -- driver_types.h:553

   --  skipped empty struct cudaArray

   subtype cudaMemoryType is unsigned;
   cudaMemoryTypeHost : constant cudaMemoryType := 1;
   cudaMemoryTypeDevice : constant cudaMemoryType := 2;  -- driver_types.h:572

   type cudaMemcpyKind is
     (cudaMemcpyHostToHost,
      cudaMemcpyHostToDevice,
      cudaMemcpyDeviceToHost,
      cudaMemcpyDeviceToDevice,
      cudaMemcpyDefault);
   pragma Convention (C, cudaMemcpyKind);  -- driver_types.h:582

   type cudaPitchedPtr is record
      ptr : System.Address;  -- driver_types.h:598
      pitch : aliased stddef_h.size_t;  -- driver_types.h:599
      xsize : aliased stddef_h.size_t;  -- driver_types.h:600
      ysize : aliased stddef_h.size_t;  -- driver_types.h:601
   end record;
   pragma Convention (C_Pass_By_Copy, cudaPitchedPtr);  -- driver_types.h:596

   type cudaExtent is record
      width : aliased stddef_h.size_t;  -- driver_types.h:611
      height : aliased stddef_h.size_t;  -- driver_types.h:612
      depth : aliased stddef_h.size_t;  -- driver_types.h:613
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExtent);  -- driver_types.h:609

   type cudaPos is record
      x : aliased stddef_h.size_t;  -- driver_types.h:623
      y : aliased stddef_h.size_t;  -- driver_types.h:624
      z : aliased stddef_h.size_t;  -- driver_types.h:625
   end record;
   pragma Convention (C_Pass_By_Copy, cudaPos);  -- driver_types.h:621

   type cudaMemcpy3DParms is record
      srcArray : System.Address;  -- driver_types.h:634
      srcPos : aliased cudaPos;  -- driver_types.h:635
      srcPtr : aliased cudaPitchedPtr;  -- driver_types.h:636
      dstArray : System.Address;  -- driver_types.h:638
      dstPos : aliased cudaPos;  -- driver_types.h:639
      dstPtr : aliased cudaPitchedPtr;  -- driver_types.h:640
      extent : aliased cudaExtent;  -- driver_types.h:642
      kind : aliased cudaMemcpyKind;  -- driver_types.h:643
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemcpy3DParms);  -- driver_types.h:632

   type cudaMemcpy3DPeerParms is record
      srcArray : System.Address;  -- driver_types.h:652
      srcPos : aliased cudaPos;  -- driver_types.h:653
      srcPtr : aliased cudaPitchedPtr;  -- driver_types.h:654
      srcDevice : aliased int;  -- driver_types.h:655
      dstArray : System.Address;  -- driver_types.h:657
      dstPos : aliased cudaPos;  -- driver_types.h:658
      dstPtr : aliased cudaPitchedPtr;  -- driver_types.h:659
      dstDevice : aliased int;  -- driver_types.h:660
      extent : aliased cudaExtent;  -- driver_types.h:662
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemcpy3DPeerParms);  -- driver_types.h:650

   --  skipped empty struct cudaGraphicsResource

   subtype cudaGraphicsRegisterFlags is unsigned;
   cudaGraphicsRegisterFlagsNone : constant cudaGraphicsRegisterFlags := 0;
   cudaGraphicsRegisterFlagsReadOnly : constant cudaGraphicsRegisterFlags := 1;
   cudaGraphicsRegisterFlagsWriteDiscard : constant cudaGraphicsRegisterFlags := 2;
   cudaGraphicsRegisterFlagsSurfaceLoadStore : constant cudaGraphicsRegisterFlags := 4;  -- driver_types.h:675

   type cudaGraphicsMapFlags is
     (cudaGraphicsMapFlagsNone,
      cudaGraphicsMapFlagsReadOnly,
      cudaGraphicsMapFlagsWriteDiscard);
   pragma Convention (C, cudaGraphicsMapFlags);  -- driver_types.h:687

   type cudaGraphicsCubeFace is
     (cudaGraphicsCubeFacePositiveX,
      cudaGraphicsCubeFaceNegativeX,
      cudaGraphicsCubeFacePositiveY,
      cudaGraphicsCubeFaceNegativeY,
      cudaGraphicsCubeFacePositiveZ,
      cudaGraphicsCubeFaceNegativeZ);
   pragma Convention (C, cudaGraphicsCubeFace);  -- driver_types.h:698

   type cudaPointerAttributes is record
      memoryType : aliased cudaMemoryType;  -- driver_types.h:717
      device : aliased int;  -- driver_types.h:728
      devicePointer : System.Address;  -- driver_types.h:734
      hostPointer : System.Address;  -- driver_types.h:740
   end record;
   pragma Convention (C_Pass_By_Copy, cudaPointerAttributes);  -- driver_types.h:711

   type cudaFuncAttributes is record
      sharedSizeBytes : aliased stddef_h.size_t;  -- driver_types.h:754
      constSizeBytes : aliased stddef_h.size_t;  -- driver_types.h:760
      localSizeBytes : aliased stddef_h.size_t;  -- driver_types.h:765
      maxThreadsPerBlock : aliased int;  -- driver_types.h:772
      numRegs : aliased int;  -- driver_types.h:777
      ptxVersion : aliased int;  -- driver_types.h:784
      binaryVersion : aliased int;  -- driver_types.h:791
   end record;
   pragma Convention (C_Pass_By_Copy, cudaFuncAttributes);  -- driver_types.h:747

   type cudaFuncCache is
     (cudaFuncCachePreferNone,
      cudaFuncCachePreferShared,
      cudaFuncCachePreferL1);
   pragma Convention (C, cudaFuncCache);  -- driver_types.h:798

   type cudaComputeMode is
     (cudaComputeModeDefault,
      cudaComputeModeExclusive,
      cudaComputeModeProhibited,
      cudaComputeModeExclusiveProcess);
   pragma Convention (C, cudaComputeMode);  -- driver_types.h:809

   type cudaLimit is
     (cudaLimitStackSize,
      cudaLimitPrintfFifoSize,
      cudaLimitMallocHeapSize);
   pragma Convention (C, cudaLimit);  -- driver_types.h:821

   type cudaOutputMode is
     (cudaKeyValuePair,
      cudaCSV);
   pragma Convention (C, cudaOutputMode);  -- driver_types.h:832

   subtype anon621_anon623_array is Interfaces.C.char_array (0 .. 255);
   type anon621_anon625_array is array (0 .. 2) of aliased int;
   type anon621_anon629_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp is record
      name : aliased anon621_anon623_array;  -- driver_types.h:844
      totalGlobalMem : aliased stddef_h.size_t;  -- driver_types.h:845
      sharedMemPerBlock : aliased stddef_h.size_t;  -- driver_types.h:846
      regsPerBlock : aliased int;  -- driver_types.h:847
      warpSize : aliased int;  -- driver_types.h:848
      memPitch : aliased stddef_h.size_t;  -- driver_types.h:849
      maxThreadsPerBlock : aliased int;  -- driver_types.h:850
      maxThreadsDim : aliased anon621_anon625_array;  -- driver_types.h:851
      maxGridSize : aliased anon621_anon625_array;  -- driver_types.h:852
      clockRate : aliased int;  -- driver_types.h:853
      totalConstMem : aliased stddef_h.size_t;  -- driver_types.h:854
      major : aliased int;  -- driver_types.h:855
      minor : aliased int;  -- driver_types.h:856
      textureAlignment : aliased stddef_h.size_t;  -- driver_types.h:857
      deviceOverlap : aliased int;  -- driver_types.h:858
      multiProcessorCount : aliased int;  -- driver_types.h:859
      kernelExecTimeoutEnabled : aliased int;  -- driver_types.h:860
      integrated : aliased int;  -- driver_types.h:861
      canMapHostMemory : aliased int;  -- driver_types.h:862
      computeMode : aliased int;  -- driver_types.h:863
      maxTexture1D : aliased int;  -- driver_types.h:864
      maxTexture2D : aliased anon621_anon629_array;  -- driver_types.h:865
      maxTexture3D : aliased anon621_anon625_array;  -- driver_types.h:866
      maxTexture1DLayered : aliased anon621_anon629_array;  -- driver_types.h:867
      maxTexture2DLayered : aliased anon621_anon625_array;  -- driver_types.h:868
      surfaceAlignment : aliased stddef_h.size_t;  -- driver_types.h:869
      concurrentKernels : aliased int;  -- driver_types.h:870
      ECCEnabled : aliased int;  -- driver_types.h:871
      pciBusID : aliased int;  -- driver_types.h:872
      pciDeviceID : aliased int;  -- driver_types.h:873
      pciDomainID : aliased int;  -- driver_types.h:874
      tccDriver : aliased int;  -- driver_types.h:875
      asyncEngineCount : aliased int;  -- driver_types.h:876
      unifiedAddressing : aliased int;  -- driver_types.h:877
      memoryClockRate : aliased int;  -- driver_types.h:878
      memoryBusWidth : aliased int;  -- driver_types.h:879
      l2CacheSize : aliased int;  -- driver_types.h:880
      maxThreadsPerMultiProcessor : aliased int;  -- driver_types.h:881
   end record;
   pragma Convention (C_Pass_By_Copy, cudaDeviceProp);  -- driver_types.h:842

   subtype cudaError_t is cudaError;

   --  skipped empty struct CUstream_st

   type cudaStream_t is new System.Address;  -- driver_types.h:942

   --  skipped empty struct CUevent_st

   type cudaEvent_t is new System.Address;  -- driver_types.h:948

   type cudaGraphicsResource_t is new System.Address;  -- driver_types.h:954

   --  skipped empty struct CUuuid_st

   --  skipped empty struct cudaUUID_t

   subtype cudaOutputMode_t is cudaOutputMode;

end driver_types_h;
