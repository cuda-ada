--
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  University of Applied Sciences Rapperswil
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with CUDA;
with CUDA.Logger;

pragma Elaborate_All (CUDA);

package body CUDA.Autoinit is

   package L renames CUDA.Logger;

   -------------------------------------------------------------------------

   overriding
   procedure Finalize (T : in out Autoinit_Type)
   is
   begin
      Check_Result (Code => cuda_h.cuCtxDestroy_v2 (arg1 => T.Context),
                    Msg  => "Context destroy failed");
      pragma Debug (L.Log ("CUDA context destroyed"));
   end Finalize;

   -------------------------------------------------------------------------

   overriding
   procedure Initialize (T : in out Autoinit_Type)
   is
   begin
      Check_Result (Code => cuda_h.cuInit (arg1 => 0),
                    Msg  => "CUDA init failed");
      Check_Result (Code => cuda_h.cuDeviceGet
                    (arg1 => T.Device'Access,
                     arg2 => 0),
                    Msg  => "Could not get device");
      Check_Result
        (Code => cuda_h.cuCtxCreate_v2
           (arg1 => T.Context'Address,
            arg2 => 0,
            arg3 => T.Device),
         Msg  => "Context creation failed");
      pragma Debug (L.Log ("CUDA context created"));
   end Initialize;

   -------------------------------------------------------------------------

   Instance : Autoinit_Type;
   --  Autoinit singleton.

   pragma Unreferenced (Instance);

end CUDA.Autoinit;
